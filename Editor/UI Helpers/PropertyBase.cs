﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace ABXY.Reflex.InputEditor.UIHelpers
{
    public class PropertyBase
    {

        public void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            SerializedObject so = new SerializedObject(property.objectReferenceValue);
            so.Update();
            OnGUI(position, property, so, label);
            so.ApplyModifiedProperties();
            EditorGUI.EndProperty();
        }

        protected virtual void OnGUI(Rect position, SerializedProperty property, SerializedObject so, GUIContent label)
        {

        }

        public float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedObject so = new SerializedObject(property.objectReferenceValue);
            so.Update();
            float height = GetPropertyHeight(property, so, label);
            so.ApplyModifiedProperties();
            return height;
        }

        protected virtual float GetPropertyHeight(SerializedProperty property, SerializedObject so, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

        private static Dictionary<System.Type, PropertyBase> propCache = new Dictionary<System.Type, PropertyBase>();

        public static PropertyBase FindPropertyForType(System.Type searchType)
        {
            if (propCache.ContainsKey(searchType))
                return propCache[searchType];
            foreach (Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (System.Type type in assembly.GetTypes())
                {
                    var attribs = type.GetCustomAttributes(typeof(PropertyDecorator), false);
                    if (attribs != null && attribs.Length > 0)
                    {
                        foreach (System.Attribute attr in attribs)
                        {
                            if ((attr as PropertyDecorator).type.Equals(searchType))
                            {
                                PropertyBase propBase = (PropertyBase)System.Activator.CreateInstance(type);
                                propCache.Add(searchType, propBase);
                                return propBase;
                            }
                        }
                    }
                }
            }
            return null;
        }
    }
}