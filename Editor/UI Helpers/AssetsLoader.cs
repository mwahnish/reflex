﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[InitializeOnLoad]
public static class AssetsLoader
{
    static AssetsLoader()
    {
        if (!Directory.Exists(Path.Combine( Application.dataPath, "Reflex Assets")))
        {
            
            Debug.Log("Loading reflex assets ");
            string packageCache = Path.Combine(Directory.GetParent(Application.dataPath).ToString(), "Library", "PackageCache");
            string reflexRootDir = "";
            foreach (string dirName in Directory.EnumerateDirectories(packageCache))
            {
                if (dirName.Contains("com.abxy.reflex"))
                    reflexRootDir = dirName;
            }
            if (reflexRootDir != "")
            {
                string assetsPath = Path.Combine(reflexRootDir, "Reflex Assets.unitypackage");
                if (File.Exists(assetsPath))
                    AssetDatabase.ImportPackage(assetsPath, false);
            }

        }
        
    }
}
