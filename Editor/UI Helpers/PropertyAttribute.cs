﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputEditor.UIHelpers
{
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class PropertyDecorator : System.Attribute
    {
        public System.Type type { get; private set; }

        public PropertyDecorator(System.Type type)
        {
            this.type = type;
        }
    }
}