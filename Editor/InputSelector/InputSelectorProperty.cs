﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using abxy.editorhelpers;
using System.Reflection;

[CustomPropertyDrawer(typeof(InputSelector))]
public class InputSelectorProperty : SPEditor
{
    private static List<System.Type> cachedControllerEnums = new List<System.Type>();

    protected override void DrawGUI(Rect position)
    {
        Rect labelRect = new Rect(position.x, position.y, EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight);

        if (data.label.text.Equals(""))
            labelRect.width = 0f;
        else
            EditorGUI.LabelField(labelRect, data.label);

        Rect controlRect = new Rect(labelRect.width + position.x, position.y, (position.width - labelRect.width) / 2f,
            EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing);
        System.Type enumType = DrawControllerEnumSelector(controlRect);

        controlRect.x += (position.width - labelRect.width) / 2f;
        DrawEnumValueSelector(controlRect, enumType);
    }

    private System.Type DrawControllerEnumSelector(Rect position)
    {
        // getting current value
        SerializedProperty currentControllerEnum = data.so.FindProperty("controllerEnumName");
        string currentControllerEnumName = currentControllerEnum.stringValue;

        // getting currently loaded controllers
        List<System.Type> controllerEnums = GetControllerEnums();
        List<string> controllerEnumNames = GetEnumNames(controllerEnums);

        // getting current index
        int currentIndex = controllerEnumNames.FindIndex(x => x.Equals(currentControllerEnumName));
        currentIndex = Mathf.Clamp(currentIndex, 0, controllerEnumNames.Count - 1);

        // displaying popup
        int newIndex = EditorGUI.Popup(position, currentIndex, controllerEnumNames.ToArray());
        currentControllerEnum.stringValue = controllerEnumNames[newIndex];

        return controllerEnums[newIndex];
    }

    private void DrawEnumValueSelector(Rect position, System.Type enumType)
    {
        // getting enum value names
        List<string> enumNames = new List<string>(enumType.GetEnumNames());

        // getting current enum value
        SerializedProperty currentInputEnum = data.so.FindProperty("inputEnumName");
        string currentInputEnumName = currentInputEnum.stringValue;
        int currentIndex = enumNames.FindIndex(x => x.Equals(currentInputEnumName));
        currentIndex = Mathf.Clamp(currentIndex, 0, enumNames.Count - 1);

        // drawing enum popup
        int newIndex = EditorGUI.Popup(position, currentIndex, enumNames.ToArray());
        currentInputEnum.stringValue = enumNames[newIndex];
    }

    private List<string> GetEnumNames(List<System.Type> enums)
    {
        List<string> enumNames = new List<string>();
        foreach (System.Type enumType in enums)
            enumNames.Add(enumType.Name);
        return enumNames;
    }

    private List<System.Type> GetControllerEnums()
    {
        List<System.Type> enums = new List<System.Type>();

        if (cachedControllerEnums.Count == 0)
        {
            foreach (Assembly asm in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                System.Type[] types = asm.GetTypes();
                foreach (var type in types)
                {
                    if (type != null && type.Namespace != null && type.Namespace.Equals("Controllers") && type.IsEnum)
                    {
                        enums.Add(type);
                    }
                }
            }
            cachedControllerEnums = enums;
        }
        else
            enums = cachedControllerEnums;
        return enums;
    }
}
