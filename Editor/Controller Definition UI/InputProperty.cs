﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;

namespace ABXY.Reflex.InputEditor.ControllerDefinitionUI
{
    [CustomPropertyDrawer(typeof(InputDefinition))]
    public class InputProperty : PropertyDrawer
    {

        private static Dictionary<object, SerializedObject> sos = new Dictionary<object, SerializedObject>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            float indent = 10f;
            Rect controlPosition = new Rect(position.x + indent, position.y, position.width - indent, EditorGUIUtility.singleLineHeight);

            if (property != null && property.objectReferenceValue != null)
            {

                if (!sos.ContainsKey(property.objectReferenceValue))
                {
                    sos.Add(property.objectReferenceValue, new SerializedObject(property.objectReferenceValue));
                }
                SerializedObject so = sos[property.objectReferenceValue];

                SerializedProperty expanded = so.FindProperty("expanded");



                controlPosition.y += EditorGUIUtility.standardVerticalSpacing;

                expanded.boolValue = EditorGUI.Foldout(new Rect(controlPosition.x, controlPosition.y, 10f, controlPosition.height), expanded.boolValue, "");

                if (expanded.boolValue)
                {

                    EditorGUI.PropertyField(controlPosition, so.FindProperty("inputName"));

                    controlPosition.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                    EditorGUI.PropertyField(controlPosition, so.FindProperty("inputType"));

                    controlPosition.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                    SerializedProperty inputType = so.FindProperty("inputType");
                    DrawInputSpecificControls(controlPosition, so, (Enums.InputTypes)inputType.enumValueIndex);
                }
                else
                {
                    EditorGUI.LabelField(controlPosition, so.FindProperty("inputName").stringValue);
                }
            }
            EditorGUI.EndProperty();
        }

        private Rect DrawInputSpecificControls(Rect position, SerializedObject so, Enums.InputTypes inputType)
        {
            float lineHeight = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            switch (inputType)
            {
                case Enums.InputTypes.Button:
                    EditorGUI.PropertyField(position, so.FindProperty("positive"));
                    break;
                case Enums.InputTypes.DigitalAxis:
                    EditorGUI.PropertyField(position, so.FindProperty("positive"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("negative"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("gravity"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("sensitivity"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("snap"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("invert"));
                    break;
                case Enums.InputTypes.MouseAxis:
                    EditorGUI.PropertyField(position, so.FindProperty("sensitivity"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("invertX"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("invertY"));
                    position.y += lineHeight;
                    break;
                case Enums.InputTypes.AnalogAxis:
                    EditorGUI.PropertyField(position, so.FindProperty("axis"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("sensitivity"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("deadZone"));
                    position.y += lineHeight;

                    EditorGUI.PropertyField(position, so.FindProperty("invert"));
                    break;
                case Enums.InputTypes.AnalogButton:
                    Rect axisRect = new Rect(position.x, position.y, position.width / 3, position.height);
                    Rect operationRect = new Rect(position.x + (position.width / 3), position.y, position.width / 3, position.height);
                    Rect cutoffRect = new Rect(position.x + 2 * (position.width / 3), position.y, position.width / 3, position.height);

                    EditorGUI.PropertyField(axisRect, so.FindProperty("axis"), new GUIContent(""));

                    EditorGUI.PropertyField(operationRect, so.FindProperty("analogButtonOperationType"), new GUIContent(""));

                    EditorGUI.PropertyField(cutoffRect, so.FindProperty("analogButtonCutoff"), new GUIContent(""));

                    break;
                case Enums.InputTypes.ProgrammaticInput:
                    SerializedProperty serializedCustomAxisDel = so.FindProperty("serializedCustomAxisDel");
                    ProgrammaticInputAttribute.ProgrammaticAxisInfo currentAxis = ProgrammaticInputAttribute.FindAxisByName(serializedCustomAxisDel.stringValue);

                    //getting dropdown index of current delegate
                    int currentIndex = ProgrammaticInputAttribute.FindIndexOfProgrammaticInput(currentAxis);
                    List<string> allDelegates = ProgrammaticInputAttribute.allDelegateNames;
                    int newCurrentIndex = EditorGUI.Popup(position, Mathf.Clamp(currentIndex, 0, allDelegates.Count - 1), allDelegates.ToArray());
                    ProgrammaticInputAttribute.ProgrammaticAxisInfo currentAttributeInfo = ProgrammaticInputAttribute.GetProgrammaticAxisByIndex(newCurrentIndex);
                    serializedCustomAxisDel.stringValue = currentAttributeInfo.delegateName;
                    position.y += lineHeight;

                    if (currentAttributeInfo.attributeData.showAxisSelector)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("axis"));
                        position.y += lineHeight;
                    }


                    if (currentAttributeInfo.attributeData.showSensitivity)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("sensitivity"));
                        position.y += lineHeight;
                    }

                    if (currentAttributeInfo.attributeData.showDeadZone)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("deadZone"));
                        position.y += lineHeight;
                    }

                    if (currentAttributeInfo.attributeData.showInvert)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("invert"));
                        position.y += lineHeight;
                    }

                    if (currentAttributeInfo.attributeData.showPositiveButtonSelector)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("positive"));
                        position.y += lineHeight;
                    }

                    if (currentAttributeInfo.attributeData.showNegativeButtonSelector)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("negative"));
                        position.y += lineHeight;
                    }

                    if (currentAttributeInfo.attributeData.showGravity)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("gravity"));
                        position.y += lineHeight;
                    }

                    if (currentAttributeInfo.attributeData.showSnap)
                    {
                        EditorGUI.PropertyField(position, so.FindProperty("snap"));
                        position.y += lineHeight;
                    }

                    break;

            }
            position.y += lineHeight;
            return position;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float spacing = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

            if (property != null && property.objectReferenceValue != null)
            {
                if (!sos.ContainsKey(property.objectReferenceValue))
                {
                    sos.Add(property.objectReferenceValue, new SerializedObject(property.objectReferenceValue));
                }
                SerializedObject so = sos[property.objectReferenceValue];

                SerializedProperty expanded = so.FindProperty("expanded");
                if (expanded.boolValue)
                {
                    SerializedProperty inputType = so.FindProperty("inputType");
                    float height = spacing * 2f + 2f * EditorGUIUtility.standardVerticalSpacing;
                    height = GetInputSpecificControlHeights(so, height, (Enums.InputTypes)inputType.enumValueIndex);
                    return height;
                }
                else
                {
                    return spacing;
                }
            }
            return spacing;

        }

        private float GetInputSpecificControlHeights(SerializedObject so, float height, Enums.InputTypes inputType)
        {
            float lineHeight = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            switch (inputType)
            {
                case Enums.InputTypes.Button:
                    height += lineHeight;
                    break;
                case Enums.InputTypes.DigitalAxis:
                    height += 6f * lineHeight;
                    break;
                case Enums.InputTypes.MouseAxis:
                    height += 3f * lineHeight;
                    break;
                case Enums.InputTypes.AnalogAxis:
                    height += 4f * lineHeight;
                    break;
                case Enums.InputTypes.AnalogButton:
                    height += lineHeight;
                    break;
                case Enums.InputTypes.ProgrammaticInput:
                    SerializedProperty serializedCustomAxisDel = so.FindProperty("serializedCustomAxisDel");
                    ProgrammaticInputAttribute.ProgrammaticAxisInfo currentAxis = ProgrammaticInputAttribute.FindAxisByName(serializedCustomAxisDel.stringValue);

                    height += lineHeight;
                    if (currentAxis.attributeData != null)
                    {
                        if (currentAxis.attributeData.showAxisSelector)
                            height += lineHeight;
                        if (currentAxis.attributeData.showSensitivity)
                            height += lineHeight;
                        if (currentAxis.attributeData.showDeadZone)
                            height += lineHeight;
                        if (currentAxis.attributeData.showInvert)
                            height += lineHeight;
                        if (currentAxis.attributeData.showPositiveButtonSelector)
                            height += lineHeight;
                        if (currentAxis.attributeData.showNegativeButtonSelector)
                            height += lineHeight;
                        if (currentAxis.attributeData.showGravity)
                            height += lineHeight;
                        if (currentAxis.attributeData.showSnap)
                            height += lineHeight;
                    }
                    break;

            }
            return height;
        }

        public static void ApplyChanges()
        {
            foreach (KeyValuePair<object, SerializedObject> kv in sos)
            {
                if (kv.Value != null)
                    kv.Value.ApplyModifiedProperties();
            }
            sos.Clear();
        }

        public static void ClearChanges()
        {
            sos.Clear();
        }
    }
}