﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Malee.Editor;
using abxy.CodeIO;
using System.IO;
using System.Text.RegularExpressions;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;

namespace ABXY.Reflex.InputEditor.ControllerDefinitionUI
{
    [CustomEditor(typeof(ControllerType))]
    public class ControllerTypeEditor : Editor
    {

        //ReorderableList inputList;
        ReorderableList idStrings;
        ReorderableList platforms;

        private static List<Platform> newPlatforms = new List<Platform>();
        private static List<Platform> removedPlatforms = new List<Platform>();

        private bool changed = false;

        private void OnEnable()
        {
            EditorApplication.projectChanged += CheckIfMoved;
            serializedObject.Update();

            idStrings = new ReorderableList(serializedObject.FindProperty("idStrings"));
            platforms = new ReorderableList(serializedObject.FindProperty("platforms"));
            platforms.onAddCallback += AddPlatform;
            platforms.onRemoveCallback += RemovePlatform;
            changed = false;
        }

        private void OnDisable()
        {
            DoUnsavedChangesDialog();
            /*
            serializedObject.Update();
            PlatformPropertyDrawer.ClearChanges();
            InputProperty.ClearChanges();
            */
        }

        private bool DoUnsavedChangesDialog()
        {
            bool wasChanged = changed;
            if (changed)
            {
                bool apply = EditorUtility.DisplayDialog("Unsaved changes", "Your changes are unsaved", "Apply", "revert");

                if (apply)
                {
                    Apply();
                }
                else
                {
                    Reset();

                }
            }

            changed = false;
            return wasChanged;
        }

        public override void OnInspectorGUI()
        {

            EditorGUI.BeginChangeCheck();

            idStrings.DoLayoutList();
            platforms.DoLayoutList();


            if (GUILayout.Button("Copy"))
            {
                CopySelected();
            }

            EditorGUILayout.Space();



            GUILayout.FlexibleSpace();


            if (EditorGUI.EndChangeCheck())
            {
                changed = true;
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Save"))
            {
                Apply();
            }

            EditorGUILayout.EndHorizontal();



            //inputList.DoLayoutList();
        }

        private void CopySelected()
        {
            List<Platform> itemsToCopy = new List<Platform>();
            foreach (int selectedIndex in platforms.Selected)
            {
                itemsToCopy.Add(platforms.GetItem(selectedIndex).objectReferenceValue as Platform);
            }
            foreach (Platform platform in itemsToCopy)
            {
                Platform newPlatform = platform.Clone();
                platforms.AddItem<Platform>(newPlatform);
                newPlatforms.Add(newPlatform);
            }
            //platforms.List.serializedObject.ApplyModifiedProperties();
        }

        private void AddPlatform(ReorderableList list)
        {
            Platform newElement = Platform.Make(target as ControllerType);
            newPlatforms.Add(newElement);

            newElement.name = "Platform";
            AssetDatabase.AddObjectToAsset(newElement, target);
            list.AddItem<Platform>(newElement);
            //serializedObject.ApplyModifiedProperties();
            //AssetDatabase.SaveAssets();
        }

        private void RemovePlatform(ReorderableList list)
        {
            List<Object> selectedElements = new List<Object>();
            foreach (int item in list.Selected)
            {
                selectedElements.Add(list.List.GetArrayElementAtIndex(item).objectReferenceValue);
            }

            list.Remove(list.Selected);

            foreach (Object item in selectedElements.ToArray())
            {
                removedPlatforms.Add(item as Platform);
            }
            //serializedObject.ApplyModifiedProperties();
            //serializedObject.Update();
            //AssetDatabase.SaveAssets();
        }

        private void Apply()
        {
            changed = false;
            serializedObject.ApplyModifiedProperties();
            PlatformPropertyDrawer.ApplyChanges();
            InputProperty.ApplyChanges();

            int platformCount = removedPlatforms.Count;
            for (int index = 0; index < platformCount; index++)
            {
                Platform currentPlatform = removedPlatforms[index];
                removedPlatforms[index] = null;
                if (currentPlatform != null)
                    currentPlatform.Destroy();
            }
            removedPlatforms.Clear();
            newPlatforms.Clear();

            DeleteInputEnumAsset();
            MakeInputEnumAsset();

            AssetDatabase.SaveAssets();
        }

        private void MakeInputEnumAsset()
        {
            string filename = RemoveSpecialChars(target.name) + "Generated.cs";
            string filePath = Path.Combine(Path.GetDirectoryName(Application.dataPath),
                Path.GetDirectoryName(AssetDatabase.GetAssetPath(target)),
                filename);

            FileBuilder asset = new FileBuilder();

            NamespaceBuilder controllersNamespace = new NamespaceBuilder("Controllers");
            asset.AddField(controllersNamespace);

            EnumBuilder controllerEnum = new EnumBuilder(RemoveSpecialChars(serializedObject.targetObject.name));
            controllersNamespace.AddField(controllerEnum);

            List<string> inputs = GetAllInputNames();
            foreach (string input in inputs)
            {
                controllerEnum.AddValue(RemoveSpecialChars(input));
            }

            CodeBuilder.WriteToFile(filePath, asset);

            SerializedProperty oldAssetPathSP = serializedObject.FindProperty("assetPath");
            SerializedProperty enumFileName = serializedObject.FindProperty("enumFilename");

            oldAssetPathSP.stringValue = GetCurrentAssetPath();
            enumFileName.stringValue = filename;
            serializedObject.ApplyModifiedProperties();

        }

        private string RemoveSpecialChars(string input)
        {
            return Regex.Replace(input, "^[0-9]?|[^a-zA-Z0-9]+", "");
        }

        private List<string> GetAllInputNames()
        {
            List<string> inputs = new List<string>();
            SerializedProperty platforms = serializedObject.FindProperty("platforms");
            for (int index = 0; index < platforms.arraySize; index++)
            {
                SerializedProperty currentPlatform = platforms.GetArrayElementAtIndex(index);
                if (currentPlatform.objectReferenceValue != null)
                {
                    SerializedObject platform = new SerializedObject(currentPlatform.objectReferenceValue);
                    SerializedProperty serializedInputs = platform.FindProperty("inputs");
                    for (int inputIndex = 0; inputIndex < serializedInputs.arraySize; inputIndex++)
                    {
                        SerializedProperty inputDefProperty = serializedInputs.GetArrayElementAtIndex(inputIndex);
                        if (inputDefProperty.objectReferenceValue != null)
                        {
                            SerializedObject serializedInputDef = new SerializedObject(inputDefProperty.objectReferenceValue);
                            string inputName = serializedInputDef.FindProperty("inputName").stringValue;
                            if (!inputs.Contains(inputName))
                                inputs.Add(inputName);
                        }

                    }
                }
            }
            return inputs;
        }

        private void CheckIfMoved()
        {
            // The path to this asset 
            SerializedProperty oldAssetPathSP = serializedObject.FindProperty("assetPath");

            // The filename to the generated enum
            SerializedProperty enumFileName = serializedObject.FindProperty("enumFilename");

            // the old path to this asset
            string oldPath = oldAssetPathSP.stringValue;



            string currentAssetPath = GetCurrentAssetPath();

            if (currentAssetPath != oldPath)
            {
                DoUnsavedChangesDialog();
                DeleteInputEnumAsset();
                oldAssetPathSP.stringValue = currentAssetPath;
                enumFileName.stringValue = RemoveSpecialChars(target.name) + "Generated.cs";
                serializedObject.ApplyModifiedProperties();


                MakeInputEnumAsset();
            }
        }

        private void DeleteInputEnumAsset()
        {
            // The path to this asset 
            SerializedProperty oldAssetPathSP = serializedObject.FindProperty("assetPath");
            // The filename to the generated enum
            SerializedProperty enumFileName = serializedObject.FindProperty("enumFilename");
            // the old path to this asset
            string oldPath = oldAssetPathSP.stringValue;

            if (oldPath != "" && enumFileName.stringValue != "")
            {
                // the path to the old enum
                string oldEnumPath = Path.Combine(Application.dataPath, Path.GetDirectoryName(oldPath), enumFileName.stringValue);
                if (File.Exists(oldEnumPath))
                    File.Delete(oldEnumPath);
            }
        }

        private string GetCurrentAssetPath()
        {
            //removing the assets prefix
            string datapath = Regex.Replace(AssetDatabase.GetAssetPath(target),
                @"^(\/|\\)?[Aa]ssets(\/|\\)", "");
            return datapath;
        }

        private void Reset()
        {
            PlatformPropertyDrawer.ClearChanges();
            InputProperty.ClearChanges();
            int platformCount = newPlatforms.Count;
            for (int index = 0; index < platformCount; index++)
            {
                Platform currentPlatform = newPlatforms[index];
                newPlatforms[index] = null;
                if (currentPlatform != null)
                    currentPlatform.Destroy();
            }
            removedPlatforms.Clear();
            newPlatforms.Clear();
            AssetDatabase.SaveAssets();
        }

    }
}