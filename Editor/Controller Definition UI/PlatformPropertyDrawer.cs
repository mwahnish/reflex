﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Malee.Editor;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using abxy.editorhelpers;

namespace ABXY.Reflex.InputEditor.ControllerDefinitionUI
{
    [CustomPropertyDrawer(typeof(Platform))]
    public class PlatformPropertyDrawer : PropertyDrawer
    {


        private static Dictionary<object, SerializedPlatform> objects = new Dictionary<object, SerializedPlatform>();
        private static SerializedObject lastSO;

        public static List<InputDefinition> addedInputs = new List<InputDefinition>();
        public static List<InputDefinition> removedInputs = new List<InputDefinition>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            if (property.objectReferenceValue != null)
            {
                //checking if so has been reset
                if (lastSO != property.serializedObject)
                    objects.Clear();

                //Setting up data persistance if necessary
                if (!objects.ContainsKey(property.objectReferenceValue))
                {

                    SerializedObject so = new SerializedObject(property.objectReferenceValue);
                    ReorderableList inputList = new ReorderableList(so.FindProperty("inputs"));
                    lastSO = property.serializedObject;
                    inputList.onAddCallback += AddItem;
                    inputList.onRemoveCallback += RemoveItem;
                    objects.Add(property.objectReferenceValue, new SerializedPlatform(so, inputList));
                }
                SerializedPlatform serializedPlatform = objects[property.objectReferenceValue];

                Rect controlPosition = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

                controlPosition.y += EditorGUIUtility.standardVerticalSpacing;

                //Drawing enum
                SerializedProperty platform = serializedPlatform.so.FindProperty("m_platform");
                Platform.platforms platformEnum = platform.GetBaseProperty<Platform.platforms>();
                platformEnum = (Platform.platforms)EditorGUI.EnumFlagsField(controlPosition, platformEnum);
                platform.SetBaseProperty<Platform.platforms>(platformEnum);



                controlPosition.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                float inputListHeight = serializedPlatform.inputs.GetHeight();

                serializedPlatform.inputs.DoList(new Rect(controlPosition.x, controlPosition.y, controlPosition.width, inputListHeight), new GUIContent("Inputs"));
            }
            EditorGUI.EndProperty();
        }

        private struct SerializedPlatform
        {
            public SerializedObject so { get; private set; }
            public ReorderableList inputs { get; private set; }
            public SerializedPlatform(SerializedObject so, ReorderableList inputs)
            {
                this.so = so;
                this.inputs = inputs;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            //Setting up data persistance if necessary
            if (property.objectReferenceValue != null)
            {
                if (!objects.ContainsKey(property.objectReferenceValue))
                {

                    SerializedObject so = new SerializedObject(property.objectReferenceValue);
                    ReorderableList inputList = new ReorderableList(so.FindProperty("inputs"));
                    inputList.onAddCallback += AddItem;
                    inputList.onRemoveCallback += RemoveItem;
                    objects.Add(property.objectReferenceValue, new SerializedPlatform(so, inputList));
                }
                SerializedPlatform serializedPlatform = objects[property.objectReferenceValue];

                float height = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                height += 2f * EditorGUIUtility.standardVerticalSpacing;
                if (serializedPlatform.inputs != null)
                    height += serializedPlatform.inputs.GetHeight();
                return height;
            }
            else
            {
                return 0;
            }
        }

        private void AddItem(ReorderableList list)
        {
            InputDefinition newElement = InputDefinition.Make((ControllerType)list.List.serializedObject.FindProperty("owningControllerDef").objectReferenceValue);
            newElement.name = "Input";
            AssetDatabase.AddObjectToAsset(newElement, list.List.serializedObject.targetObject);
            list.AddItem<InputDefinition>(newElement);

            addedInputs.Add(newElement);

            //list.List.serializedObject.ApplyModifiedProperties();
            //AssetDatabase.SaveAssets();
        }

        private void RemoveItem(ReorderableList list)
        {
            List<Object> selectedElements = new List<Object>();
            foreach (int item in list.Selected)
            {
                selectedElements.Add(list.List.GetArrayElementAtIndex(item).objectReferenceValue);
            }

            list.Remove(list.Selected);

            foreach (Object item in selectedElements.ToArray())
            {
                removedInputs.Add(item as InputDefinition);
            }
            //list.List.serializedObject.ApplyModifiedProperties();
            //list.List.serializedObject.Update();
            //AssetDatabase.SaveAssets();
        }

        public static void ApplyChanges()
        {
            foreach (KeyValuePair<object, SerializedPlatform> kv in objects)
            {
                kv.Value.so.ApplyModifiedProperties();
            }
            objects.Clear();

            int inputCount = removedInputs.Count;
            for (int index = 0; index < inputCount; index++)
            {
                InputDefinition currentInputDefinition = removedInputs[index];
                removedInputs[index] = null;
                if (currentInputDefinition != null)
                    currentInputDefinition.Destroy();
            }
            removedInputs.Clear();
            addedInputs.Clear();
            AssetDatabase.SaveAssets();

        }

        public static void ClearChanges()
        {
            objects.Clear();

            int inputCount = addedInputs.Count;
            for (int index = 0; index < inputCount; index++)
            {
                InputDefinition currentInput = addedInputs[index];
                addedInputs[index] = null;
                if (currentInput != null)
                    currentInput.Destroy();
            }
            removedInputs.Clear();
            addedInputs.Clear();
            AssetDatabase.SaveAssets();
        }
    }
}