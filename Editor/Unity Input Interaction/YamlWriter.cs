﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputEditor.UnityInputInteraction
{
    public class YamlWriter
    {
        List<YamlEntityBase> entities = new List<YamlEntityBase>();

        public YamlEntityBase Add(YamlEntityBase newEntity)
        {
            entities.Add(newEntity);
            return newEntity;
        }

        public void Write(string filePath)
        {
            List<string> lines = new List<string>();
            foreach (YamlEntityBase entity in entities)
            {
                lines = entity.Write(lines, -1);
            }
            System.IO.File.WriteAllLines(filePath, lines);
        }
    }


    public abstract class YamlEntityBase
    {
        public virtual List<string> Write(List<string> currentLines, int currentIndent)
        {
            return currentLines;
        }

        protected string Indent(int amount)
        {
            string indent = "";
            for (int index = 0; index < amount; index++)
            {
                indent += "  ";
            }
            return indent;
        }


    }

    public abstract class KeyedEntityBase : YamlEntityBase
    {

        public string key { get; protected set; }
        protected void IsRepresentedAsObject(List<string> currentLines)
        {
            if (key == "")
                Debug.LogError("Object at line " + currentLines.Count + " is being written like an object, but has no key");
        }

        public virtual List<string> WriteAsArrayElement(List<string> currentLines, int currentIndent, int index)
        {
            return currentLines;
        }
    }

    public class YamlDirective : YamlEntityBase
    {
        public string directive { get; private set; }
        public string directivePrefix { get; private set; }
        public YamlDirective(string directive)
        {
            this.directive = directive;
            this.directivePrefix = "%";
        }

        public YamlDirective(string directive, string prefix)
        {
            this.directive = directive;
            this.directivePrefix = prefix;
        }

        public override List<string> Write(List<string> currentLines, int currentIndent)
        {
            currentIndent++;
            currentLines.Add(Indent(currentIndent) + directivePrefix + directive);
            return currentLines;
        }
    }

    public class YamlVar : KeyedEntityBase
    {
        public string value { get; protected set; }

        public YamlVar(string key, string value)
        {
            this.key = key;
            this.value = value;
        }

        public YamlVar(string value)
        {
            this.value = value;
        }

        public YamlVar(string key, int value)
        {
            this.key = key;
            this.value = value.ToString();
        }

        public YamlVar(int value)
        {
            this.value = value.ToString();
        }

        public YamlVar(string key, float value)
        {
            this.key = key;
            this.value = value.ToString();
        }

        public YamlVar(float value)
        {
            this.value = value.ToString();
        }

        public override List<string> Write(List<string> currentLines, int currentIndent)
        {
            IsRepresentedAsObject(currentLines);
            currentIndent++;
            currentLines.Add(Indent(currentIndent) + key + ": " + value);
            return currentLines;
        }

        public override List<string> WriteAsArrayElement(List<string> currentLines, int currentIndent, int index)
        {
            IsRepresentedAsObject(currentLines);
            currentIndent++;
            if (index == 0)
                currentLines.Add(Indent(currentIndent - 1) + "- " + key + ": " + value);
            else
                currentLines.Add(Indent(currentIndent) + key + ": " + value);
            return currentLines;
        }

    }

    public class YamlObject : KeyedEntityBase
    {
        protected List<KeyedEntityBase> subEntities = new List<KeyedEntityBase>();

        public YamlObject(string key)
        {
            this.key = key;
        }
        public YamlObject()
        {

        }

        public virtual KeyedEntityBase AddSubEntity(KeyedEntityBase subEntity)
        {
            subEntities.Add(subEntity);
            return subEntity;
        }

        public override List<string> Write(List<string> currentLines, int currentIndent)
        {
            IsRepresentedAsObject(currentLines);
            currentIndent++;
            currentLines.Add(Indent(currentIndent) + key + ":");
            foreach (KeyedEntityBase subEntity in subEntities)
                currentLines = subEntity.Write(currentLines, currentIndent);
            return currentLines;
        }

        public override List<string> WriteAsArrayElement(List<string> currentLines, int currentIndent, int index)
        {
            //currentIndent++;
            for (int i = 0; i < subEntities.Count; i++)
                currentLines = subEntities[i].WriteAsArrayElement(currentLines, currentIndent, i);
            return currentLines;
        }
    }

    public class YamlArray : YamlObject
    {
        public YamlArray(string key) : base(key)
        {
            this.key = key;
        }

        public override List<string> Write(List<string> currentLines, int currentIndent)
        {
            currentIndent++;
            currentLines.Add(Indent(currentIndent) + key + ":");
            for (int i = 0; i < subEntities.Count; i++)
                currentLines = subEntities[i].WriteAsArrayElement(currentLines, currentIndent, i);
            return currentLines;
        }

        public override List<string> WriteAsArrayElement(List<string> currentLines, int currentIndent, int index)
        {
            currentIndent++;
            currentLines.Add(Indent(currentIndent - 1) + "- " + key + ":");
            for (int i = 0; i < subEntities.Count; i++)
                currentLines = subEntities[i].WriteAsArrayElement(currentLines, currentIndent, i);
            return currentLines;
        }
    }


}