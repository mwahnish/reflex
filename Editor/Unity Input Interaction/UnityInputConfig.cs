﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.Reflex.InputRuntime;

namespace ABXY.Reflex.InputEditor.UnityInputInteraction
{
    public static class UnityInputConfig
    {
        public static void RewriteUnityInputDefinition()
        {
            SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
            SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");
            axesProperty.arraySize = 0;
            serializedObject.ApplyModifiedProperties();

            MakeButton("ControlsWrittenFlag", "");

            string[] buttonNames = System.Enum.GetNames(typeof(Enums.ButtonNames));
            for (int i = 0; i < GetNumbersOfButtons() - 1; i++)
            {
                MakeButton(buttonNames[i], Utilities.ButtonEnumToUnityString((Enums.ButtonNames)i));
            }

            // doing mouse
            MakeMouseAxis("MouseX", Enums.Axes.AxisX);
            MakeMouseAxis("MouseY", Enums.Axes.AxisY);

            for (int joystickNum = 1; joystickNum < 12; joystickNum++)
            {
                for (int axes = 1; axes < System.Enum.GetNames(typeof(Enums.Axes)).Length + 1; axes++)
                {
                    string axesName = (axes).ToString();
                    if (axes == 1)
                        axesName = "X";
                    else if (axes == 2)
                        axesName = "Y";
                    MakeAnalogAxis("Joystick" + joystickNum + "Axis" + axesName, joystickNum, axes - 1);
                }

                int numberOfButtons = GetNumbersOfButtons();
                int numberJoystickButtons = GetNumbersOfJoystickButtons();
                for (int i = numberOfButtons; i < numberOfButtons + numberJoystickButtons; i++)
                {
                    int joystickNumber = i - numberOfButtons;
                    MakeButton("Joystick" + joystickNum + buttonNames[i], Utilities.ButtonEnumToUnityString((Enums.ButtonNames)i), joystickNum);
                }
            }

        }

        public static bool AreInputsWritten()
        {
            return DoesAxisExist("ControlsWrittenFlag");
        }

        public static bool DoesAxisExist(string axisName)
        {
            SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
            SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");
            for (int index = 0; index < axesProperty.arraySize; index++)
            {
                SerializedProperty arrayElement = axesProperty.GetArrayElementAtIndex(index);
                SerializedProperty name = GetChildProperty(arrayElement, "m_Name");
                if (name.stringValue == axisName)
                    return true;
            }
            return false;
        }

        public enum AxisType
        {
            KeyOrMouseButton = 0,
            MouseMovement = 1,
            JoystickAxis = 2
        };

        private static int GetNumbersOfButtons()
        {
            return (int)Enums.ButtonNames.joystickButton0;
        }

        private static int GetNumbersOfJoystickButtons()
        {
            string[] buttonNames = System.Enum.GetNames(typeof(Enums.ButtonNames));
            return buttonNames.Length - GetNumbersOfButtons();
        }

        private static void AddInput(string name, string descriptiveName, string descriptiveNegativeName, string negativeButton,
            string positiveButton, string altNegativeButton, string altPositiveButton, float gravity, float dead, float sensitivity,
            bool snap, bool invert, AxisType type, int axis, int joyNum)
        {
            SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
            SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");

            axesProperty.arraySize++;
            serializedObject.ApplyModifiedProperties();

            SerializedProperty newAxis = axesProperty.GetArrayElementAtIndex(axesProperty.arraySize - 1);

            GetChildProperty(newAxis, "m_Name").stringValue = name;
            GetChildProperty(newAxis, "descriptiveName").stringValue = descriptiveName;
            GetChildProperty(newAxis, "descriptiveNegativeName").stringValue = descriptiveNegativeName;
            GetChildProperty(newAxis, "negativeButton").stringValue = negativeButton;
            GetChildProperty(newAxis, "positiveButton").stringValue = positiveButton;
            GetChildProperty(newAxis, "altNegativeButton").stringValue = altNegativeButton;
            GetChildProperty(newAxis, "altPositiveButton").stringValue = altPositiveButton;
            GetChildProperty(newAxis, "gravity").floatValue = gravity;
            GetChildProperty(newAxis, "dead").floatValue = dead;
            GetChildProperty(newAxis, "sensitivity").floatValue = sensitivity;
            GetChildProperty(newAxis, "snap").boolValue = snap;
            GetChildProperty(newAxis, "invert").boolValue = invert;
            GetChildProperty(newAxis, "type").intValue = (int)type;
            GetChildProperty(newAxis, "axis").intValue = axis;
            GetChildProperty(newAxis, "joyNum").intValue = joyNum;
            serializedObject.ApplyModifiedPropertiesWithoutUndo();
            //GetChildProperty(newAxis, "") so.ApplyModifiedPropertiesWithoutUndo();
        }

        private static SerializedProperty GetChildProperty(SerializedProperty parent, string name)
        {
            SerializedProperty child = parent.Copy();
            child.Next(true);
            do
            {
                if (child.name == name) return child;
            }
            while (child.Next(false));
            return null;
        }

        private static void MakeButton(string name, string positiveButton)
        {
            AddInput(name, "", "", "", positiveButton, "", "", 1000f, 0.001f, 1000, false, false, AxisType.KeyOrMouseButton, 0, 0);
        }

        private static void MakeButton(string name, string positiveButton, int joystickNum)
        {
            positiveButton = positiveButton.Replace("joystick ", "joystick " + joystickNum + " ");
            AddInput(name, "", "", "", positiveButton, "", "", 1000f, 0.001f, 1000, false, false, AxisType.KeyOrMouseButton, 0, joystickNum);
        }

        private static void MakeMouseAxis(string name, Enums.Axes mouseAxis)
        {
            int axis = 0;
            if (mouseAxis == Enums.Axes.AxisY)
                axis = 1;
            AddInput(name, "", "", "", "", "", "", 0f, 0f, 0.1f, false, false, AxisType.MouseMovement, axis, 0);
        }

        private static void MakeAnalogAxis(string name, int joystickNumber, int axis)
        {
            AddInput(name, "", "", "", "", "", "", 3f, 0.001f, 1, false, false, AxisType.JoystickAxis, axis, joystickNumber);
        }


    }
}