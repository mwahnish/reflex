﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using abxy.editorhelpers;
using Malee.Editor;
namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    [CustomPropertyDrawer(typeof(JoinKeyCombo))]
    public class KeycomboPropertyDrawer : SPEditor
    {
        protected override void DrawGUI(Rect position)
        {
            InitializeList();
            ReorderableList list = data.Get<ReorderableList>("keyComboList");

            Rect listPosition = new Rect(position.x, position.y + 2f * EditorGUIUtility.standardVerticalSpacing, position.width, list.GetHeight());
            list.DoList(listPosition, new GUIContent("Key combo"));

        }

        protected override float CalculatePropertyHeight()
        {
            InitializeList();
            ReorderableList list = data.Get<ReorderableList>("keyComboList");
            return list.GetHeight() + 4f *EditorGUIUtility.standardVerticalSpacing;
        }

        private void InitializeList()
        {
            if (!data.HasObject<ReorderableList>("keyComboList"))
            {
                SerializedProperty listProp = data.so.FindProperty("keyComboList");
                ReorderableList list = new ReorderableList(listProp);
                list.onAddCallback += OnAdd;
                list.onRemoveCallback += OnRemove;
                list.elementLabels = false;
                data.Set("keyComboList", list);
            }
        }


        private void OnAdd(ReorderableList list)
        {
            InputSelector newKeyCombo = ScriptableObject.CreateInstance<InputSelector>();
            data.AddSubasset(newKeyCombo, true);
            list.AddItem<InputSelector>(newKeyCombo);
        }

        private void OnRemove(ReorderableList list)
        {
            foreach (int removeIndex in list.Selected)
            {
                data.RemoveSubasset(list.List.GetArrayElementAtIndex(removeIndex).objectReferenceValue);
            }
            list.Remove(list.Selected);
        }
    }
}