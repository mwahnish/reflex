﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using abxy.editorhelpers;

namespace ABXY.Reflex.InputEditor.ControlManagerUI
{
    [CustomPropertyDrawer(typeof(InputAxis))]
    public class InputAxisPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.objectReferenceValue != null)
            {
                EditorGUI.BeginProperty(position, label, property);
                SerializedObject so = new SerializedObject(property.objectReferenceValue);
                Enums.InputTypes inputType = so.FindProperty("inputType").GetBaseProperty<Enums.InputTypes>();

                EditorGUI.LabelField(position, so.FindProperty("axisName").stringValue);

                if (inputType == Enums.InputTypes.Button)
                {
                    string buttonState = (property.objectReferenceValue as InputAxis).buttonState.ToString();
                    float width = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label.CalcSize(new GUIContent(buttonState)).x;
                    Rect valueLabelRect = new Rect(position.x + position.width - width, position.y, width, position.height);
                    EditorGUI.LabelField(valueLabelRect, buttonState);
                }
                else if (inputType == Enums.InputTypes.AnalogAxis)
                {
                    string value = (property.objectReferenceValue as InputAxis).axisValue.ToString();
                    float width = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label.CalcSize(new GUIContent(value)).x;
                    Rect valueLabelRect = new Rect(position.x + position.width - width, position.y, width, position.height);
                    EditorGUI.LabelField(valueLabelRect, value);
                }
                else if (inputType == Enums.InputTypes.DigitalAxis)
                {
                    string value = (property.objectReferenceValue as InputAxis).axisValue.ToString();
                    float width = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label.CalcSize(new GUIContent(value)).x;
                    Rect valueLabelRect = new Rect(position.x + position.width - width, position.y, width, position.height);
                    EditorGUI.LabelField(valueLabelRect, value);
                }
                else if (inputType == Enums.InputTypes.AnalogButton)
                {
                    string boolValue = (property.objectReferenceValue as InputAxis).buttonState.ToString();

                    float boolValueWidth = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label.CalcSize(new GUIContent(boolValue)).x;

                    Rect boolValueLabelRect = new Rect(position.x + position.width - boolValueWidth, position.y, boolValueWidth, position.height);

                    EditorGUI.LabelField(boolValueLabelRect, boolValue);
                }
                else if (inputType == Enums.InputTypes.ProgrammaticInput)
                {
                    string boolValue = (property.objectReferenceValue as InputAxis).buttonState.ToString();
                    string floatValue = (property.objectReferenceValue as InputAxis).axisValue.ToString();
                    string vectorValue = (property.objectReferenceValue as InputAxis).vectorAxisValue.ToString();

                    float boolValueWidth = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label.CalcSize(new GUIContent(boolValue)).x;
                    float floatValueWidth = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label.CalcSize(new GUIContent(floatValue)).x;
                    float vectorValueWidth = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).label.CalcSize(new GUIContent(vectorValue)).x;

                    Rect boolValueLabelRect = new Rect(position.x + position.width - boolValueWidth - floatValueWidth - vectorValueWidth, position.y, boolValueWidth, position.height);
                    Rect floatValueLabelRect = new Rect(position.x + position.width - vectorValueWidth - floatValueWidth, position.y, floatValueWidth, position.height);
                    Rect vectorValueLabelRect = new Rect(position.x + position.width - vectorValueWidth, position.y, vectorValueWidth, position.height);

                    EditorGUI.LabelField(boolValueLabelRect, boolValue);
                    EditorGUI.LabelField(floatValueLabelRect, floatValue);
                    EditorGUI.LabelField(vectorValueLabelRect, vectorValue);
                }



                EditorGUI.EndProperty();

            }
        }
    }
}