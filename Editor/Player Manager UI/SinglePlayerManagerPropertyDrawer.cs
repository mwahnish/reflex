﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ABXY.Reflex.InputEditor.UIHelpers;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;

namespace ABXY.Reflex.InputEditor.ControlManagerUI
{
    [PropertyDecorator(typeof(SinglePlayerManager))]
    public class SinglePlayerManagerPropertyDrawer : PropertyBase
    {
        protected override void OnGUI(Rect position, SerializedProperty property, SerializedObject so, GUIContent label)
        {
            Rect controlPosition = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

            EditorGUI.LabelField(controlPosition, "Single Player Mode");
        }

        protected override float GetPropertyHeight(SerializedProperty property, SerializedObject so, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

    }
}
