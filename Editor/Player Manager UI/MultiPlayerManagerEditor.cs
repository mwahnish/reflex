﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ABXY.Reflex.InputEditor.UIHelpers;
using Malee.Editor;
using abxy.editorhelpers;
using abxy.ubugger.EditorExtensions;
using UnityEngine.Profiling;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    [CustomEditor(typeof(MultiPlayerManager))]
    public class MultiPlayerManagerEditor : SOEditor
    {

        ReorderableList joinKeycodesList;
        ReorderableList exitKeycodesList;

        protected override void DoOnAwake()
        {
            InitializeSubasset(serializedObject.FindProperty("testInputSelector"));
            showApplyRevert = true;
        }

        protected override void DoOnEnable()
        {
            joinKeycodesList = new ReorderableList(serializedObject.FindProperty("joinKeyCombos"));
            joinKeycodesList.onAddCallback += OnAddKeycombo;
            joinKeycodesList.onRemoveCallback += OnRemoveKeyCombo;
            joinKeycodesList.label = new GUIContent("Join key combos");

            exitKeycodesList = new ReorderableList(serializedObject.FindProperty("exitKeyCombos"));
            exitKeycodesList.onAddCallback += OnAddKeycombo;
            exitKeycodesList.onRemoveCallback += OnRemoveKeyCombo;
            exitKeycodesList.label = new GUIContent("Exit key combos");
        }

        protected override void DoInspectorGUI()
        {
            joinKeycodesList.DoLayoutList();
            exitKeycodesList.DoLayoutList();
            this.DrawLogBar(target);
            Profiler.EndSample();
        }

        private void OnAddKeycombo(ReorderableList list)
        {
            JoinKeyCombo newKeyCombo = ScriptableObject.CreateInstance<JoinKeyCombo>();
            AddSubasset(newKeyCombo,true);
            list.AddItem<JoinKeyCombo>(newKeyCombo);
        }

        private void OnRemoveKeyCombo (ReorderableList list)
        {
            foreach (int removeIndex in list.Selected)
            {
                RemoveSubasset( list.List.GetArrayElementAtIndex(removeIndex).objectReferenceValue);
            }
            list.Remove(list.Selected);
            
        }
    }
}
