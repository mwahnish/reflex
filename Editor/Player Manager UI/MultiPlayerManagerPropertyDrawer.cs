﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ABXY.Reflex.InputEditor.UIHelpers;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using Malee.Editor;

namespace ABXY.Reflex.InputEditor.ControlManagerUI
{
    [PropertyDecorator(typeof(MultiPlayerManager))]
    public class MultiPlayerManagerPropertyDrawer : PropertyBase
    {
        Dictionary<Object, ReorderableList> lists = new Dictionary<Object, ReorderableList>();
        protected override void OnGUI(Rect position, SerializedProperty property, SerializedObject so, GUIContent label)
        {
            Rect controlPosition = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
            InitializeListIfNeeded(property, so);

            // drawing keycodes
            ReorderableList list = lists[property.objectReferenceValue];
            float listHeight = list.GetHeight();
            list.DoList(
                new Rect(controlPosition.x, controlPosition.y, controlPosition.width, listHeight),
                new GUIContent(""));
            controlPosition.y += EditorGUIUtility.standardVerticalSpacing;
            controlPosition.y += listHeight;

            EditorGUI.LabelField(controlPosition, "Multiple Player Mode");
        }

        protected override float GetPropertyHeight(SerializedProperty property, SerializedObject so, GUIContent label)
        {
            InitializeListIfNeeded(property, so);
            ReorderableList list = lists[property.objectReferenceValue];
            float listHeight = list.GetHeight();
            return EditorGUIUtility.singleLineHeight + listHeight + EditorGUIUtility.standardVerticalSpacing ;
        }

        private void InitializeListIfNeeded(SerializedProperty property, SerializedObject so)
        {
            if (property.objectReferenceValue != null)
            {
                if (!lists.ContainsKey(property.objectReferenceValue))
                    lists.Add(property.objectReferenceValue, new ReorderableList(so.FindProperty("keyCombos")));
            }
        }
    }
}
