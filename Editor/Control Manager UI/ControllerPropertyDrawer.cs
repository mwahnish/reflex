﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;

namespace ABXY.Reflex.InputEditor.ControlManagerUI
{
    [CustomPropertyDrawer(typeof(Controller))]
    public class ControllerPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            
            EditorGUI.BeginProperty(position, label, property);

            if (property != null && property.objectReferenceValue != null)
            {
                Rect controlRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

                EditorGUI.LabelField(controlRect, (property.objectReferenceValue as Controller).controllerName
                    + " - Input " + (property.objectReferenceValue as Controller).controllerNumber);

                float buttonWidth = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).button.CalcSize(new GUIContent("View")).x;
                Rect buttonRect = new Rect(position.x + position.width - buttonWidth, position.y, buttonWidth, EditorGUIUtility.singleLineHeight);

                if (GUI.Button(buttonRect, "View"))
                {
                    ControllerInputView.Make(property);
                }
            }
            EditorGUI.EndProperty();
        }
    }
}
