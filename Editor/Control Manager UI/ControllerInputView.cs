﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Malee.Editor;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;

namespace ABXY.Reflex.InputEditor.ControlManagerUI
{
    public class ControllerInputView : EditorWindow
    {
        private SerializedObject controller;
        ReorderableList inputsView;

        public static ControllerInputView Make(SerializedProperty controller)
        {
            // ControllerInputView window = (ControllerInputView)EditorWindow.GetWindow(typeof(ControllerInputView));
            ControllerInputView window = ScriptableObject.CreateInstance<ControllerInputView>();
            window.controller = new SerializedObject(controller.objectReferenceValue);
            window.inputsView = new ReorderableList(window.controller.FindProperty("inputsList"));
            window.inputsView.paginate = true;
            window.inputsView.pageSize = 40;
            window.inputsView.canAdd = false;
            window.inputsView.canRemove = false;
            window.inputsView.sortable = false;

            window.titleContent = new GUIContent((controller.objectReferenceValue as Controller).controllerName
                + " - Input " + (controller.objectReferenceValue as Controller).controllerNumber);
            window.autoRepaintOnSceneChange = true;
            window.Show();
            return window;
        }

        private void OnGUI()
        {
            if (controller == null || controller.targetObject == null)
            {
                this.Close();
                return;
            }
            if (inputsView != null)
                inputsView.DoLayoutList();
            Repaint();
        }
    }
}
