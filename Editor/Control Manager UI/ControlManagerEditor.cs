﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Malee.Editor;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.Reflex.InputEditor.UnityInputInteraction;
namespace ABXY.Reflex.InputEditor.ControlManagerUI
{
    [CustomEditor(typeof(ControlManager))]
    public class ControlManagerEditor : Editor
    {
        ReorderableList allowedControllerTypes;
        ReorderableList inherentControllers;
        ReorderableList availableControllers;
        ReorderableList undefinedControllers;

        public void OnEnable()
        {
            serializedObject.Update();
            allowedControllerTypes = new ReorderableList(serializedObject.FindProperty("allowedControllerTypes"));

            inherentControllers = new ReorderableList(serializedObject.FindProperty("inherentControllerTypes"));

            availableControllers = new ReorderableList(serializedObject.FindProperty("availableControllers"));
            availableControllers.canAdd = false;
            availableControllers.canRemove = false;
            availableControllers.draggable = false;

            undefinedControllers = new ReorderableList(serializedObject.FindProperty("undefinedControllers"));
            undefinedControllers.drawElementCallback += DrawUndefinedController;
            undefinedControllers.canAdd = false;
            undefinedControllers.canRemove = false;
            undefinedControllers.draggable = false;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            bool inputsAreWritten = UnityInputConfig.AreInputsWritten();

            // Edit mode
            if (!Application.isPlaying)
            {
                if (inputsAreWritten)
                {
                    if (GUILayout.Button("Reset inputs"))
                        UnityInputConfig.RewriteUnityInputDefinition();
                    EditorGUILayout.HelpBox("Inputs have been generated. This button will regenerate all your inputs", MessageType.None);

                    EditorGUILayout.Space();

                    allowedControllerTypes.DoLayoutList();

                    EditorGUILayout.Space();
                    EditorGUILayout.HelpBox("Put inputs that are assumed to be always connected here, I.E. systen devices like keyboards, mice, etc.", MessageType.None);
                    inherentControllers.DoLayoutList();

                }
                else
                {
                    if (GUILayout.Button("Generate Input"))
                        UnityInputConfig.RewriteUnityInputDefinition();
                    EditorGUILayout.HelpBox("This button will delete all your current inputs", MessageType.Info);
                }
            }

            //Play Mode
            if (Application.isPlaying)
            {
                availableControllers.DoLayoutList();

                if (undefinedControllers.List.arraySize > 0)
                {
                    EditorGUILayout.Space();
                    EditorGUILayout.HelpBox("The following inputs do not have an associated controller definition, or is not included as an allowed controller", MessageType.Info);
                    undefinedControllers.DoLayoutList();
                }

            }

            // Editor or play mode

            if (inputsAreWritten)
            {
                EditorGUILayout.Space();
                EditorGUILayout.PropertyField(serializedObject.FindProperty("playerManager"));

            }

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("persistAcrossScenes"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("supersedesOthers"));
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("mode"));
            serializedObject.ApplyModifiedProperties();

        }


        private void DrawUndefinedController(Rect rect, SerializedProperty element, GUIContent label, bool selected, bool focused)
        {
            EditorGUI.SelectableLabel(rect, element.stringValue);
        }
    }
}