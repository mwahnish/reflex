﻿using ABXY.Reflex;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinglePlayerController : MonoBehaviour
{

    PlayerInfo player;



    [SerializeField]
    private float force = 5f;

    private InputSubscriber forward = new InputSubscriber(Controllers.Keyboard.WSAxis, Controllers.Xbox360Controller.LeftStickVertical,Controllers.XboxOneController.LeftStickVertical);
    private InputSubscriber right = new InputSubscriber(Controllers.Keyboard.ADAxis, Controllers.Xbox360Controller.LeftStickHorizontal, Controllers.XboxOneController.LeftStickHorizontal);

    // Start is called before the first frame update
    void Start()
    {
        ControlManager.AddOnPlayerSpawnedListener(OnPlayerAdded);
        forward.SetPlayerIndependentInput();
        right.SetPlayerIndependentInput();
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 velocity = new Vector3(right.inputData.axisValue, 0f, forward.inputData.axisValue) * force;


        Rigidbody rigidBody = GetComponent<Rigidbody>();
        rigidBody.velocity = transform.TransformDirection( new Vector3(velocity.x, rigidBody.velocity.y, velocity.z));
    }
    

    private void OnPlayerAdded(ReflexEvents.OnPlayerInfoCreated e)
    {
        player = e.newPlayer;
        forward.SetPlayer(e.newPlayer);
        right.SetPlayer(e.newPlayer);
    }

    
}
