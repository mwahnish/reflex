﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;

namespace ABXY.Reflex.InputRuntime
{
    public class Utilities : MonoBehaviour
    {
        private static Dictionary<Enums.ButtonNames, string> enumToString;

        public static string ButtonEnumToUnityString(Enums.ButtonNames buttonEnum)
        {
            if (enumToString == null)
                SetUpEnumToStringMapping();
            string result = "";

            if (enumToString.ContainsKey(buttonEnum))
            {
                result = enumToString[buttonEnum];
            }
            else
            {
                result = Regex.Replace(buttonEnum.ToString(), @"(\p{Lu})", " $1").TrimStart().ToLower();
                result = Regex.Replace(result, @"(?<=[a-z])[0-9]{1,2}\Z", " $0");
            }
            return result;
        }

        private static void SetUpEnumToStringMapping()
        {
            enumToString = new Dictionary<Enums.ButtonNames, string>();
            enumToString.Add(Enums.ButtonNames.backspace, "backspace");
            enumToString.Add(Enums.ButtonNames.delete, "delete");
            enumToString.Add(Enums.ButtonNames.tab, "tab");
            enumToString.Add(Enums.ButtonNames.enter, "return");
            enumToString.Add(Enums.ButtonNames.clear, "clear");
            enumToString.Add(Enums.ButtonNames.pause, "pause");
            enumToString.Add(Enums.ButtonNames.escape, "escape");
            enumToString.Add(Enums.ButtonNames.spacebar, "space");
            enumToString.Add(Enums.ButtonNames.up, "up");
            enumToString.Add(Enums.ButtonNames.down, "down");
            enumToString.Add(Enums.ButtonNames.right, "right");
            enumToString.Add(Enums.ButtonNames.left, "left");
            enumToString.Add(Enums.ButtonNames.insert, "insert");
            enumToString.Add(Enums.ButtonNames.home, "home");
            enumToString.Add(Enums.ButtonNames.end, "end");
            enumToString.Add(Enums.ButtonNames.pageUp, "page up");
            enumToString.Add(Enums.ButtonNames.pageDown, "page down");
            enumToString.Add(Enums.ButtonNames.f1, "f1");
            enumToString.Add(Enums.ButtonNames.f2, "f2");
            enumToString.Add(Enums.ButtonNames.f3, "f3");
            enumToString.Add(Enums.ButtonNames.f4, "f4");
            enumToString.Add(Enums.ButtonNames.f5, "f5");
            enumToString.Add(Enums.ButtonNames.f6, "f6");
            enumToString.Add(Enums.ButtonNames.f7, "f7");
            enumToString.Add(Enums.ButtonNames.f8, "f8");
            enumToString.Add(Enums.ButtonNames.f9, "f9");
            enumToString.Add(Enums.ButtonNames.f10, "f10");
            enumToString.Add(Enums.ButtonNames.f11, "f11");
            enumToString.Add(Enums.ButtonNames.f12, "f12");
            enumToString.Add(Enums.ButtonNames.f13, "f13");
            enumToString.Add(Enums.ButtonNames.f14, "f14");
            enumToString.Add(Enums.ButtonNames.f15, "f15");
            enumToString.Add(Enums.ButtonNames.zero, "0");
            enumToString.Add(Enums.ButtonNames.one, "1");
            enumToString.Add(Enums.ButtonNames.two, "2");
            enumToString.Add(Enums.ButtonNames.three, "3");
            enumToString.Add(Enums.ButtonNames.four, "4");
            enumToString.Add(Enums.ButtonNames.five, "5");
            enumToString.Add(Enums.ButtonNames.six, "6");
            enumToString.Add(Enums.ButtonNames.seven, "7");
            enumToString.Add(Enums.ButtonNames.eight, "8");
            enumToString.Add(Enums.ButtonNames.nine, "9");
            enumToString.Add(Enums.ButtonNames.exclaimationMark, "!");
            enumToString.Add(Enums.ButtonNames.backslash, @"\");
            enumToString.Add(Enums.ButtonNames.pound, "#");
            enumToString.Add(Enums.ButtonNames.dollarSign, "$");
            enumToString.Add(Enums.ButtonNames.ampersand, "&");
            enumToString.Add(Enums.ButtonNames.singleQuote, "'");
            enumToString.Add(Enums.ButtonNames.leftParen, "(");
            enumToString.Add(Enums.ButtonNames.rightParen, ")");
            enumToString.Add(Enums.ButtonNames.star, "*");
            enumToString.Add(Enums.ButtonNames.plus, "+");
            enumToString.Add(Enums.ButtonNames.comma, ",");
            enumToString.Add(Enums.ButtonNames.minus, "-");
            enumToString.Add(Enums.ButtonNames.period, ".");
            enumToString.Add(Enums.ButtonNames.forwardSlash, @"/");
            enumToString.Add(Enums.ButtonNames.colon, ":");
            enumToString.Add(Enums.ButtonNames.semicolon, ";");
            enumToString.Add(Enums.ButtonNames.lessThan, "<");
            enumToString.Add(Enums.ButtonNames.equals, "=");
            enumToString.Add(Enums.ButtonNames.greaterThan, ">");
            enumToString.Add(Enums.ButtonNames.questionMark, "?");
            enumToString.Add(Enums.ButtonNames.at, "@");
            enumToString.Add(Enums.ButtonNames.leftBracket, "[");
            enumToString.Add(Enums.ButtonNames.rightBracket, "]");
            enumToString.Add(Enums.ButtonNames.caret, "^");
            enumToString.Add(Enums.ButtonNames.underscore, "_");
            enumToString.Add(Enums.ButtonNames.tilde, "`");
            enumToString.Add(Enums.ButtonNames.a, "a");
            enumToString.Add(Enums.ButtonNames.b, "b");
            enumToString.Add(Enums.ButtonNames.c, "c");
            enumToString.Add(Enums.ButtonNames.d, "d");
            enumToString.Add(Enums.ButtonNames.e, "e");
            enumToString.Add(Enums.ButtonNames.f, "f");
            enumToString.Add(Enums.ButtonNames.g, "g");
            enumToString.Add(Enums.ButtonNames.h, "h");
            enumToString.Add(Enums.ButtonNames.i, "i");
            enumToString.Add(Enums.ButtonNames.j, "j");
            enumToString.Add(Enums.ButtonNames.k, "k");
            enumToString.Add(Enums.ButtonNames.l, "l");
            enumToString.Add(Enums.ButtonNames.m, "m");
            enumToString.Add(Enums.ButtonNames.n, "n");
            enumToString.Add(Enums.ButtonNames.o, "o");
            enumToString.Add(Enums.ButtonNames.p, "p");
            enumToString.Add(Enums.ButtonNames.q, "q");
            enumToString.Add(Enums.ButtonNames.r, "r");
            enumToString.Add(Enums.ButtonNames.s, "s");
            enumToString.Add(Enums.ButtonNames.t, "t");
            enumToString.Add(Enums.ButtonNames.u, "u");
            enumToString.Add(Enums.ButtonNames.v, "v");
            enumToString.Add(Enums.ButtonNames.w, "w");
            enumToString.Add(Enums.ButtonNames.x, "x");
            enumToString.Add(Enums.ButtonNames.y, "y");
            enumToString.Add(Enums.ButtonNames.z, "z");
            enumToString.Add(Enums.ButtonNames.numlock, "numlock");
            enumToString.Add(Enums.ButtonNames.caps, "caps lock");
            enumToString.Add(Enums.ButtonNames.scrollLock, "scroll lock");
            enumToString.Add(Enums.ButtonNames.rightShift, "right shift");
            enumToString.Add(Enums.ButtonNames.leftShift, "left shift");
            enumToString.Add(Enums.ButtonNames.rightCtrl, "right ctrl");
            enumToString.Add(Enums.ButtonNames.leftCtrl, "left ctrl");
            enumToString.Add(Enums.ButtonNames.rightAlt, "right alt");
            enumToString.Add(Enums.ButtonNames.leftAlt, "left alt");
        }

        public static string RemoveSpecialChars(string input)
        {
            return Regex.Replace(input, "^[0-9]?|[^a-zA-Z0-9]+", "");
        }
    }
}