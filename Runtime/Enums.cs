﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex
{
    public class Enums
    {
        public enum InputTypes { Button, MouseAxis, DigitalAxis, AnalogAxis, AnalogButton, ProgrammaticInput }

        public enum ButtonNames
        {
            zero, one, two, three, four, five, six, seven, eight, nine, minus, equals,
            backspace, delete, tab, caps, leftShift, rightShift, leftCtrl, rightCtrl, leftAlt, rightAlt, leftBracket,
            rightBracket, backslash, tilde, semicolon, singleQuote, comma, period, forwardSlash, enter, left,
            right, up, down, spacebar, clear, pause, escape, insert, home,

            end, pageUp, pageDown, exclaimationMark, pound, dollarSign,
            ampersand, leftParen, rightParen, star, plus, colon, lessThan,
            greaterThan, questionMark, at, caret, underscore,
            numlock, scrollLock,
            f1, f2, f3, f4, f5, f6, f7, f8, f9, f10,
            f11, f12, f13, f14, f15,

            a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x,
            y, z, None, joystickButton0, joystickButton1, joystickButton2, joystickButton3, joystickButton4,
            joystickButton5, joystickButton6, joystickButton7, joystickButton8, joystickButton9, joystickButton10,
            joystickButton11, joystickButton12
        }



        public enum Axes
        {
            AxisX, AxisY, Axis3, Axis4, Axis5, Axis6, Axis7, Axis8, Axis9, Axis10, Axis11, Axis12,
            Axis13, Axis14, Axis15, Axis16, Axis17, Axis18, Axis19, Axis20, Axis21, Axis22, Axis23, Axis24, Axis25,
            Axis26, Axis27, Axis28
        }
    }
}