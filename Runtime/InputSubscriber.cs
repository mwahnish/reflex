﻿using ABXY.Reflex;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Reflex.Internal;

public class InputSubscriber
{
    private System.Enum[] inputs;

    private PlayerInfo player;

    public InputChange inputData { get; private set; }

    public UnityEngine.Events.UnityAction onButtonDown;

    public UnityEngine.Events.UnityAction onButtonUp;

    public UnityEngine.Events.UnityAction whileButtonDown;

    public UnityEngine.Events.UnityAction<InputSubscriber> onChange;

    List<int> controllerNumbers = new List<int>();
    

    public InputSubscriber(params System.Enum[] inputs)
    {
        this.inputs = inputs;
    }

    public void SetPlayerIndependentInput()
    {
        foreach (int controllerNumber in controllerNumbers)
        {
            ReflexInternalEvents.GetControllerReference.Answer answer = 
                ControlManager.query.AskSingle<ReflexInternalEvents.GetControllerReference.Answer>(new ReflexInternalEvents.GetControllerReference(controllerNumber));
            answer?.controller?.UnsubscribeToInput(OnInputChange);
        }
        controllerNumbers.Clear();

        ControlManager.events.AddListener< ReflexEvents.OnControllerAssigned>(OnControllerAdd);
        ControlManager.events.AddListener<ReflexEvents.OnControllerUnassigned>(OnControllerRemove);

        int[] connectedControllers = PlayerManagerBase.GetAllControllerNumbers();
        foreach (int controllerNumber in connectedControllers)
        {
            SubscribeToController(controllerNumber);
        }
    }

    public void SetController(int controllerNumber)
    {
        SubscribeToController(controllerNumber);
    }

    public void SetPlayer( PlayerInfo player)
    {
        foreach (int controllerNumber in controllerNumbers)
        {
            ReflexInternalEvents.GetControllerReference.Answer answer =
                ControlManager.query.AskSingle<ReflexInternalEvents.GetControllerReference.Answer>(new ReflexInternalEvents.GetControllerReference(controllerNumber));
            answer?.controller?.UnsubscribeToInput(OnInputChange);
        }
        controllerNumbers.Clear();
        this.player = player;
        ControlManager.events.AddListener<ReflexEvents.OnControllerAssigned>(OnControllerAdd, x=>x.assignedTo.playerGuid == player.playerGuid);
        ControlManager.events.AddListener<ReflexEvents.OnControllerUnassigned>(OnControllerRemove, x => x.assignedTo.playerGuid == player.playerGuid);
        int[] previouslyAssignedControllers = PlayerManagerBase.GetPlayerControllerNumbers(player.playerGuid);
        foreach(int previousController in previouslyAssignedControllers)
        {
            SubscribeToController(previousController);
        }
    }

    private void OnControllerAdd(ReflexEvents.OnControllerAssigned e)
    {
        SubscribeToController(e.controller);
    }

    private void OnControllerRemove(ReflexEvents.OnControllerUnassigned e)
    {
        UnsubscribeToController(e.controller);
    }

    private void SubscribeToController(int controllerNumber)
    {
        foreach(System.Enum input in inputs)
        {
            if (input != null)
            {
                ReflexInternalEvents.GetControllerReference.Answer answer =
                ControlManager.query.AskSingle<ReflexInternalEvents.GetControllerReference.Answer>(new ReflexInternalEvents.GetControllerReference(controllerNumber));
                answer?.controller?.SubscribeToInput(input, OnInputChange);
            }
                
        }
        
        if (!controllerNumbers.Contains(controllerNumber))
            controllerNumbers.Add(controllerNumber);
    }

    private void UnsubscribeToController(int controllerNumber)
    {
        ReflexInternalEvents.GetControllerReference.Answer answer =
               ControlManager.query.AskSingle<ReflexInternalEvents.GetControllerReference.Answer>(new ReflexInternalEvents.GetControllerReference(controllerNumber));
        answer?.controller ?.UnsubscribeToInput(OnInputChange);
        controllerNumbers.Remove(controllerNumber);
    }

    private void OnInputChange(ReflexEvents.OnInputChange e)
    {
        InputChange newInputData = e.inputChange;

        // on change
        if (onChange != null)
            onChange.Invoke(this);

        // doing while button down
        if (newInputData.buttonValue && inputData.buttonValue && whileButtonDown != null)
            whileButtonDown.Invoke();

        // doing on button down
        if (newInputData.buttonValue && !inputData.buttonValue && onButtonDown != null)
            onButtonDown.Invoke();

        // doing on button up
        if (!newInputData.buttonValue && inputData.buttonValue && onButtonUp != null)
            onButtonUp.Invoke();
        inputData = newInputData;
    }

    public void Destroy()
    {
        foreach (int controllerNumber in controllerNumbers)
        {
            ReflexInternalEvents.GetControllerReference.Answer answer =
               ControlManager.query.AskSingle<ReflexInternalEvents.GetControllerReference.Answer>(new ReflexInternalEvents.GetControllerReference(controllerNumber));
            answer?.controller ?.UnsubscribeToInput(OnInputChange);
        }
        ControlManager.events.RemoveListener<ReflexEvents.OnControllerAssigned>(OnControllerAdd);
        ControlManager.events.RemoveListener<ReflexEvents.OnControllerUnassigned>(OnControllerRemove);
        controllerNumbers.Clear();
    }
}
