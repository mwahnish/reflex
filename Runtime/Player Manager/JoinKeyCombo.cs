﻿using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    public class JoinKeyCombo : ScriptableObject
    {
        [SerializeField]
        List<InputSelector> keyComboList = new List<InputSelector>();

        public Instance Instantiate(int instantiateWithControllerNumber)
        {
            return new Instance(instantiateWithControllerNumber, keyComboList);
        }

        public class Instance
        {
            private List<InputSubscriber> inputs = new List<InputSubscriber>();
            private Controller controller;
            private List<InputSelector> keyComboList;

            public bool comboPressed
            {
                get
                {
                    bool pressed = true;
                    foreach (InputSubscriber input in inputs)
                    {
                        if (!input.inputData.buttonValue)
                        {
                            pressed = false;
                            break;
                        }
                    }
                    return pressed;
                }
            }

            public Instance(int controllerNumber, List<InputSelector> keyComboList)
            {
                foreach(InputSelector selector in keyComboList)
                {
                    InputSubscriber input = new InputSubscriber(selector.selectedEnum);
                    input.SetController(controllerNumber);
                    inputs.Add(input);
                }
            }

            public void Destroy()
            {
                foreach(InputSubscriber selector in inputs)
                {
                    selector.Destroy();
                }
            }
        }
    }

}