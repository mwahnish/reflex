﻿using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using ABXY.Reflex.Internal;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    [CreateAssetMenu(fileName = "SingleplayerManager", menuName = "Player Managers/Single Player Manager")]
    public class SinglePlayerManager : PlayerManagerBase
    {
        private PlayerInfo player;
        
        protected override void OnStart()
        {
            player = PlayerInfo.Make();

        }



        public override void OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange e)
        {
            if (e.status == ReflexInternalEvents.OnControllerStatusChange.Status.CONNECTED)
            {
                AssignControllerToPlayer(e.controller, player);

            }
            else
            {
                RemoveControllerFromPlayer(e.controller, player);
            }
        }
    }
}