﻿using System.Collections;
using System.Collections.Generic;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using ABXY.Reflex.Internal;
using UnityEditor;
using UnityEngine;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    [CreateAssetMenu(fileName = "MultiplayerManager", menuName = "Player Managers/Multiplayer Manager")]
    public class MultiPlayerManager : PlayerManagerBase
    {
        [SerializeField]
        List<JoinKeyCombo> joinKeyCombos = new List<JoinKeyCombo>();

        [SerializeField]
        List<JoinKeyCombo> exitKeyCombos = new List<JoinKeyCombo>();

        List<ControllerJoinController> availableControllers = new List<ControllerJoinController>();


        
        public bool acceptingNewPlayers = false;

        protected override void OnStart()
        {
            base.OnStart();
        }

        public override void Update()
        {

            
            if (acceptingNewPlayers)
            {
                foreach (ControllerJoinController cjc in availableControllers)
                    cjc.Check();
            }
        }

        public override void OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange e)
        {
            if (e.status == ReflexInternalEvents.OnControllerStatusChange.Status.DISCONNECTED)
            {
                List<System.Guid> players = GetPlayersUsingController(e.controller);
                foreach (var player in players)
                {
                    ReflexEvents.GetPlayerByID.Answer playerAnswer = ControlManager.query.AskSingle<ReflexEvents.GetPlayerByID.Answer>(
                        new ReflexEvents.GetPlayerByID(player));
                    if (playerAnswer.player != null)
                        playerAnswer.player.RemoveFromGame();
                }

            }
            else if (e.status == ReflexInternalEvents.OnControllerStatusChange.Status.CONNECTED)
                availableControllers.Add(new ControllerJoinController(e.controller, joinKeyCombos, exitKeyCombos, OnPlayerJoinCombo, OnPlayerLeaveCombo));
        }

        private void OnPlayerJoinCombo(int controller, ControllerJoinController source)
        {
            PlayerInfo playerForNewController = PlayerInfo.Make(x=> AssignControllerToPlayer(controller, x));

            
        }

        private void OnPlayerLeaveCombo(int controller, ControllerJoinController source)
        {
            //PlayerInfo playerForNewController = PlayerInfo.Make(x => AssignControllerToPlayer(controller, x));
            List<System.Guid> players = GetPlayersUsingController(controller);
            if (players.Count > 0)
            {
                System.Guid playerGUID = players[0];
                ReflexEvents.GetPlayerByID.Answer playerAnswer = ControlManager.query.AskSingle<ReflexEvents.GetPlayerByID.Answer>(
                    new ReflexEvents.GetPlayerByID(playerGUID));
                if (playerAnswer != null)
                    playerAnswer.player.RemoveFromGame();
            }

        }

        protected override void OnPlayerManagerDestroy()
        {
            availableControllers.Clear();
            acceptingNewPlayers = false;
        }

        private class ControllerJoinController
        {
            private List<JoinKeyCombo.Instance> joinKeyComboInstances = new List<JoinKeyCombo.Instance>();
            private List<JoinKeyCombo.Instance> leaveKeyComboInstances = new List<JoinKeyCombo.Instance>();
            private System.Action<int, ControllerJoinController> onJoin;
            private System.Action<int, ControllerJoinController> onLeave;

            public int controller { get; private set; }

            private bool joined = false;

            public ControllerJoinController(
                int controllerNumber, 
                List<JoinKeyCombo> joinKeyCombos, 
                List<JoinKeyCombo> leaveKeyCombos, 
                System.Action<int, ControllerJoinController> onJoin, 
                System.Action<int, ControllerJoinController> onLeave)
            {
                foreach (JoinKeyCombo combo in joinKeyCombos)
                {
                    joinKeyComboInstances.Add(combo.Instantiate(controllerNumber));
                }

                foreach (JoinKeyCombo combo in leaveKeyCombos)
                {
                    leaveKeyComboInstances.Add(combo.Instantiate(controllerNumber));
                }

                this.onJoin = onJoin;
                this.onLeave = onLeave;
                this.controller = controllerNumber;
            }

            public void Check()
            {
                if (joined)
                {
                    foreach (JoinKeyCombo.Instance instance in leaveKeyComboInstances)
                    {
                        if (instance.comboPressed)
                        {
                            joined = false;
                            onLeave?.Invoke(controller, this);
                        }
                    }
                }
                else
                {
                    foreach (JoinKeyCombo.Instance instance in joinKeyComboInstances)
                    {
                        if (instance.comboPressed)
                        {
                            joined = true;
                            onJoin?.Invoke(controller, this);
                        }
                    }
                }
            }

            public void Destroy()
            {
                foreach (JoinKeyCombo.Instance instance in joinKeyComboInstances)
                    instance.Destroy();
                foreach (JoinKeyCombo.Instance instance in leaveKeyComboInstances)
                    instance.Destroy();
            }
        }
        
    }
}