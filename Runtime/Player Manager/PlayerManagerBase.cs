﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.EventSystem;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using ABXY.Reflex.Internal;
using UnityEngine.SceneManagement;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    public class PlayerManagerBase : ScriptableObject
    {
        /// <summary>
        /// The players currently in the scene
        /// </summary>
        protected static List<PlayerInfo> players = new List<PlayerInfo>();

        public bool initialized { get; private set; }

        private static Dictionary<System.Guid, List<int>> controllerAssignments = new Dictionary<System.Guid, List<int>>();
        

        public void Setup()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneLoaded += OnSceneLoaded;
            OnSceneLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single); // assuming that if start is running, a scene was loaded

            controllerAssignments.Clear(); // making sure this doesn't persist across scene changes
            ControlManager.events.AddListener<ReflexInternalEvents.OnControllerStatusChange>(OnControllerStatusChange);
            ReflexInternalEvents.GetAllControllers.Answer controllers = ControlManager.query.AskSingle<ReflexInternalEvents.GetAllControllers.Answer>(new ReflexInternalEvents.GetAllControllers());

            ControlManager.events.AddListener<ReflexEvents.OnPlayerInfoCreated>(OnPlayerJoin);
            ControlManager.events.AddListener<ReflexEvents.OnPlayerInfoDestroyed>(OnPlayerLeave);

            OnStart();
            foreach (KeyValuePair<Controller, int> controller in controllers.controllers)
            {
                OnControllerStatusChange(new ReflexInternalEvents.OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange.Status.CONNECTED, controller.Value));
            }
            initialized = true;
        }

        protected virtual void OnStart()
        {

        }

        public virtual void Update()
        {

        }

        /// <summary>
        /// Called every time a controller is connected or disconnected from the system
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange e)
        {

        }

        private void OnPlayerJoin(ReflexEvents.OnPlayerInfoCreated e)
        {
            if (!players.Contains(e.newPlayer))
                players.Add(e.newPlayer);
        }


        private void OnPlayerLeave(ReflexEvents.OnPlayerInfoDestroyed e)
        {
            players.Remove(e.despawnedPlayer);
            controllerAssignments.Remove(e.despawnedPlayer.playerGuid);
        }

        /// <summary>
        /// Assigns a controller to the given player
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="player"></param>
        protected void AssignControllerToPlayer(int controllerNumber, PlayerInfo player)
        {
            if (controllerAssignments.ContainsKey(player.playerGuid))
            {
                List<int> controllerNumbers = controllerAssignments[player.playerGuid];
                if (!controllerNumbers.Contains(controllerNumber)) // Ensuring this isn't a duplicate add
                    controllerNumbers.Add(controllerNumber);
            }
            else
            {
                List<int> controllerNumbers = new List<int>();
                controllerNumbers.Add(controllerNumber);
                controllerAssignments.Add(player.playerGuid, controllerNumbers);
            }
            ControlManager.events.Raise(new ReflexEvents.OnControllerAssigned(controllerNumber, player));
        }

        /// <summary>
        /// Removes a controller from the player
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="player"></param>
        protected void RemoveControllerFromPlayer(int controllerNumber, PlayerInfo player)
        {
            if (controllerAssignments.ContainsKey(player.playerGuid))
            {
                List<int> controllerNumbers = controllerAssignments[player.playerGuid];
                if (controllerNumbers.Contains(controllerNumber)) // Ensuring this isn't a duplicate add
                    controllerNumbers.Remove(controllerNumber);
            }
            ControlManager.events.Raise(new ReflexEvents.OnControllerUnassigned(controllerNumber, player));
        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <returns>The players that were using this controller</returns>
        protected PlayerInfo[] RemoveControllerFromAllPlayers(int controller)
        {
            List<System.Guid> playersUsingController = new List<System.Guid>();
            foreach(KeyValuePair<System.Guid, List<int>> kv in controllerAssignments)
            {
                if (kv.Value.Contains(controller))
                    playersUsingController.Add(kv.Key);
            }
            List<PlayerInfo> returnValue = new List<PlayerInfo>();
            foreach(System.Guid playerGuid in playersUsingController)
            {
                ReflexEvents.GetPlayerByID.Answer playerAnswer =
                    ControlManager.query.AskSingle<ReflexEvents.GetPlayerByID.Answer>(new ReflexEvents.GetPlayerByID(playerGuid));
                if (playerAnswer.player != null)
                {
                    RemoveControllerFromPlayer(controller, playerAnswer.player);
                    returnValue.Add(playerAnswer.player);
                }
            }
            return returnValue.ToArray();
        }

        protected List<System.Guid> GetPlayersUsingController(Controller controller)
        {
            return GetPlayersUsingController(controller.controllerNumber);
        }

        protected List<System.Guid> GetPlayersUsingController(int controllerNumber)
        {
            List<System.Guid> players = new List<Guid>();
            foreach (var assignment in controllerAssignments)
            {
                if (assignment.Value.Contains(controllerNumber))
                    players.Add(assignment.Key);
            }

            return players;
        }

        public static int[] GetPlayerControllerNumbers(System.Guid playerID)
        {
            List<int> controllerNumbers;
            controllerAssignments.TryGetValue(playerID, out controllerNumbers);
            if (controllerNumbers == null)
                controllerNumbers = new List<int>();
            return controllerNumbers.ToArray();
        }

        public static Controller[] GetPlayerControllers(System.Guid playerId)
        {
            List<int> controllerNumbers = new List<int>(GetPlayerControllerNumbers(playerId));
            List<Controller> controllers = new List<Controller>();
            foreach (int controllerNum in controllerNumbers)
            {
                ReflexInternalEvents.GetControllerReference.Answer answer =
                    ControlManager.query.AskSingle<ReflexInternalEvents.GetControllerReference.Answer>(new ReflexInternalEvents.GetControllerReference(controllerNum));
                if (answer != null && answer.controller != null)
                    controllers.Add(answer.controller);
            }
            return controllers.ToArray();
            
        }
        

        public static int[] GetAllControllerNumbers()
        {
            ReflexInternalEvents.GetAllControllers.Answer answer =
                ControlManager.query.AskSingle<ReflexInternalEvents.GetAllControllers.Answer>(new ReflexInternalEvents.GetAllControllers());
            List<int> ids = new List<int>();
            foreach (KeyValuePair<Controller, int> kv in answer.controllers)
            {
                if (!ids.Contains(kv.Value))
                    ids.Add(kv.Value);
            }
            return ids.ToArray();
        }

        public static Controller[] GetAllControllers()
        {
            ReflexInternalEvents.GetAllControllers.Answer answer =
                ControlManager.query.AskSingle<ReflexInternalEvents.GetAllControllers.Answer>(new ReflexInternalEvents.GetAllControllers());
            List<Controller> controllers = new List<Controller>();
            foreach (KeyValuePair<Controller, int> kv in answer.controllers)
            {
                if (!controllers.Contains(kv.Key))
                    controllers.Add(kv.Key);
            }
            return controllers.ToArray();
        }

        public void Destroy()
        {
            for (int index = 0; index < players.Count; index++)
            {
                PlayerInfo player = players[index];
                if (player != null)
                    player.RemoveFromGame();
            }

            players.Clear();
            controllerAssignments.Clear();
            OnPlayerManagerDestroy();
        }

        protected virtual void OnPlayerManagerDestroy()
        {

        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            ControlManager.events.Raise(new ReflexEvents.OnSceneLoad(players.ToArray(),scene,mode));
        }

        public PlayerInfo[] GetPlayers()
        {
            return players.ToArray();
        }
    }

}