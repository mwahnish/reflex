﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class InputSelector : ScriptableObject
{
    [SerializeField]
    private string controllerEnumName = null;

    [SerializeField]
    private string inputEnumName = null;

    private System.Enum p_selectedEnum = null;
    public System.Enum selectedEnum
    {
        get
        {
            if (p_selectedEnum == null)
                UpdateEnum();
            return p_selectedEnum;
        }
        private set
        {
            p_selectedEnum = value;
        }
    }

    private void UpdateEnum()
    {
        System.Type enumType = null;
        foreach (Assembly asm in System.AppDomain.CurrentDomain.GetAssemblies())
        {
            System.Type[] types = asm.GetTypes();
            foreach (var type in types)
            {
                if (type != null &&
                    type.Name.Equals(controllerEnumName) &&
                    type.Namespace != null && 
                    type.Namespace.Equals("Controllers") && 
                    type.IsEnum)
                {
                    enumType = type;
                    break;
                }
            }
        }
        if (enumType != null)
        {
            object enumValue = System.Enum.Parse(enumType, inputEnumName);
            if (enumValue != null)
                selectedEnum = (System.Enum)enumValue;
        }
        
    }

}
