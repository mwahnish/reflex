﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.EventSystem;
using UnityEngine.SceneManagement;

namespace ABXY.Reflex
{

    public static partial class ReflexEvents
    {
        /// <summary>
        /// Called whenever a controller is assigned to a player
        /// </summary>
        public class OnControllerAssigned : GameEvent
        {
            public int controller { get; private set; }
            public PlayerInfo assignedTo { get; private set; }

            public OnControllerAssigned(int controller, PlayerInfo assignedTo)
            {
                this.controller = controller;
                this.assignedTo = assignedTo;
            }
        }

        /// <summary>
        /// Called whenever a controller is removed from a player
        /// </summary>
        public class OnControllerUnassigned : GameEvent
        {
            public int controller { get; private set; }
            public PlayerInfo assignedTo { get; private set; }

            public OnControllerUnassigned(int controller, PlayerInfo assignedTo)
            {
                this.controller = controller;
                this.assignedTo = assignedTo;
            }
        }

        public class OnInputChange : GameEvent
        {
            public InputChange inputChange { get; private set; }
            public string controllerType { get; private set; }
            public Enum inputEnum { get; private set; }
            public int controllerNumber { get; private set; }

            public OnInputChange(InputChange inputChange, string controllerType, Enum inputName, int controllerNumber)
            {
                this.inputChange = inputChange;
                this.controllerType = controllerType;
                this.inputEnum = inputName;
                this.controllerNumber = controllerNumber;
            }
        }

        public class OnPlayerInfoCreated : GameEvent
        {
            public PlayerInfo newPlayer { get; private set; }
            public OnPlayerInfoCreated(PlayerInfo newPlayer)
            {
                this.newPlayer = newPlayer;
            }
        }

        public class OnPlayerInfoDestroyed : GameEvent
        {
            public PlayerInfo despawnedPlayer { get; private set; }
            public OnPlayerInfoDestroyed(PlayerInfo despawnedPlayer)
            {
                this.despawnedPlayer = despawnedPlayer;
            }
        }

        public class OnSceneLoad :GameEvent
        {
            public PlayerInfo[] currentPlayers { get; private set; }
            public Scene newScene { get; private set; }
            public LoadSceneMode mode { get; private set; }

            public OnSceneLoad(PlayerInfo[] currentPlayers, Scene newScene, LoadSceneMode mode)
            {
                this.currentPlayers = currentPlayers;
                this.newScene = newScene;
                this.mode = mode;
            }
        }

        public class GetPlayerByID : Question
        {
            public Guid playerID { get; private set; }

            public GetPlayerByID(Guid playerID)
            {
                this.playerID = playerID;
            }

            public class Answer : QuestionAnswer
            {
                public PlayerInfo player { get; private set; }

                public Answer(PlayerInfo player)
                {
                    this.player = player;
                }
            }
        }
    }
    public struct InputChange
    {
        public float axisValue { get; private set; }
        public bool buttonValue { get; private set; }
        public Vector3 vectorValue { get; private set; }

        public InputChange(float axisValue, bool buttonValue, Vector3 vectorValue)
        {
            this.axisValue = axisValue;
            this.buttonValue = buttonValue;
            this.vectorValue = vectorValue;
        }
    }
}