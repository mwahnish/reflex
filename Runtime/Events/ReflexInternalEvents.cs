﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.EventSystem;

namespace ABXY.Reflex.Internal
{
    public static partial class ReflexInternalEvents
    {
        /// <summary>
        /// Called whenever a controller is connected or disconnected from the system.
        /// </summary>
        public class OnControllerStatusChange : GameEvent
        {
            public enum Status { CONNECTED, DISCONNECTED }
            public Status status { get; private set; }
            public int controller { get; private set; }

            public OnControllerStatusChange(Status status, int controller)
            {
                this.status = status;
                this.controller = controller;
            }
        }


        public class GetControllerReference: Question
        {
            public int controllerNumber { get; private set; }
            public GetControllerReference(int controllerNumber)
            {
                this.controllerNumber = controllerNumber;
            }

            public class Answer : QuestionAnswer
            {
                public Controller controller { get; private set; }
                public Answer (Controller controller)
                {
                    this.controller = controller;
                }
            }
        }

        public class GetAllControllers : Question
        {
            public class Answer : QuestionAnswer
            {
                public Dictionary<Controller, int> controllers { get; private set; }
                public Answer(Dictionary<Controller, int> controller)
                {
                    this.controllers = controller;
                }
            }

        }
    }
}