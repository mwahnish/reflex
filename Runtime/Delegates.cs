﻿using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputRuntime
{
    public class Delegates : MonoBehaviour
    {
        public delegate InputChange ProgrammaticAxisDel(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert);
    }
}
