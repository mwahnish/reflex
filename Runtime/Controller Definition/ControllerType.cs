﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    [CreateAssetMenu(fileName = "Controller Type")]
    public class ControllerType : ScriptableObject
    {
        [SerializeField]
        private List<string> idStrings = new List<string>();

        [SerializeField]
        private List<Platform> platforms = new List<Platform>();

#pragma warning disable CS0414
        [SerializeField]
        private string assetPath = "";
#pragma warning restore CS0414

#pragma warning disable CS0414
        [SerializeField]
        private string enumFilename = "";
#pragma warning restore CS0414

        public string SanitizedName
        {
            get { return Utilities.RemoveSpecialChars(name); }
        }

        public bool MatchesControllerNameString(string controllerName)
        {
            foreach (string id in idStrings)
            {
                if (id.Equals(controllerName))
                    return true;
            }
            return false;
        }

        public InputDefinition[] GetInputs(Platform.platforms platform)
        {
            InputDefinition[] inputs = new InputDefinition[0];
            Platform selectedPlatform = platforms.Find(x => x.platform.HasFlag(platform));
            if (selectedPlatform != null)
            {
                inputs = selectedPlatform.GetInputs();
            }

            return inputs;
        }

        public List<Platform.platforms> GetCompatiblePlatforms()
        {
            List<Platform.platforms> supportedPlatforms = new List<Platform.platforms>();
            foreach (Platform platform in platforms)
            {
                supportedPlatforms.Add(platform.platform);
            }
            return supportedPlatforms;
        }
    }
    
}
