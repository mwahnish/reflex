﻿using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    public class InputDefinition : ScriptableObject
    {

        public enum AnalogButtonOperations { GreaterThan, GreaterThanOrEqual, Equal, LessThanOrEqual, LessThan }

        

        [SerializeField]
        private ControllerType owningControllerDef;

        public static InputDefinition Make(ControllerType owningController)
        {
            InputDefinition newDef = CreateInstance<InputDefinition>();
            newDef.owningControllerDef = owningController;
            return newDef;
        }


        [SerializeField]
        private string inputName = "";

        [SerializeField]
        private Enums.InputTypes inputType = Enums.InputTypes.Button;

        [SerializeField]
        private AnalogButtonOperations analogButtonOperationType = AnalogButtonOperations.GreaterThan;

        [SerializeField]
        private Enums.Axes axis = Enums.Axes.AxisX;

        [SerializeField]
        private Enums.ButtonNames positive = Enums.ButtonNames.None;

        [SerializeField]
        private Enums.ButtonNames negative = Enums.ButtonNames.None;

        [SerializeField]
        private float gravity = 1f;

        [SerializeField]
        private float sensitivity = 1f;

        [SerializeField]
        private float deadZone = 0f;

        [SerializeField]
        private float analogButtonCutoff = 0.5f;

        [SerializeField]
        private bool snap = false;

        [SerializeField]
        private bool invert = false;

        [SerializeField]
        private bool invertX = false;

        [SerializeField]
        private bool invertY = false;

#pragma warning disable CS0414
        [SerializeField]
        private bool expanded = true;
#pragma warning restore CS0414

        private Delegates.ProgrammaticAxisDel m_customAxisDel = null;

        /// <summary>
        /// Had to set to public, since this is not serializable. Needed access in editor
        /// </summary>
        private Delegates.ProgrammaticAxisDel customAxisDel
        {
            get
            {
                if (m_customAxisDel == null && serializedCustomAxisDel != "")
                    m_customAxisDel = ProgrammaticInputAttribute.FindAxisByName(serializedCustomAxisDel).ProgrammaticAxisDel;
                return m_customAxisDel;
            }
            set
            {

                m_customAxisDel = value;
                serializedCustomAxisDel = "";
                if (value != null)
                    serializedCustomAxisDel = value.Method.ReflectedType.ToString() + "/" + value.Method.ToString();
            }
        }

        [SerializeField]
        private string serializedCustomAxisDel;

        private void OnEnable()
        {
            hideFlags = HideFlags.HideInHierarchy;
        }

        public InputDefinition Clone()
        {
            InputDefinition input = InputDefinition.Make(this.owningControllerDef);
            input.inputName = inputName;
            input.inputType = inputType;
            input.axis = axis;
            input.positive = positive;
            input.negative = negative;
            input.gravity = gravity;
            input.sensitivity = sensitivity;
            input.deadZone = deadZone;
            input.snap = snap;
            input.invert = invert;
            input.invertX = invertX;
            input.invertY = invertY;
            input.serializedCustomAxisDel = serializedCustomAxisDel;
            (input as ScriptableObject).name = "Input";
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.AssetDatabase.AddObjectToAsset(input, this);
                UnityEditor.AssetDatabase.SaveAssets();
            }
#endif
            return input;
        }

        public InputAxis MakeAxis(string controllerName, System.Type controllerInputEnumType)
        {
            System.Enum controllerInputEnum = (System.Enum)System.Enum.Parse(controllerInputEnumType, Utilities.RemoveSpecialChars(inputName));
            return InputAxis.Make(
                inputName,
                inputType,
                axis,
                positive,
                negative,
                gravity,
                sensitivity,
                deadZone,
                snap,
                invert,
                invertX,
                invertY,
                analogButtonOperationType,
                analogButtonCutoff,
                controllerName,
                controllerInputEnum,
                customAxisDel);
        }

        public void Destroy()
        {
            DestroyImmediate(this, true);
        }
        
    }
}