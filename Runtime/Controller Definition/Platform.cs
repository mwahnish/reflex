﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Reflex.InputRuntime.ControllerDefinitionSystem
{
    public class Platform : ScriptableObject
    {
        [SerializeField]
        private platforms m_platform = platforms.Nothing;

        public platforms platform { get { return m_platform; } }

        [SerializeField]
        private ControllerType owningControllerDef;

        [System.Flags]
        public enum platforms
        {
            OSXEditor = 1 << 0,
            OSXPlayer = 1 << 1,
            WindowsPlayer = 1 << 2,
            OSXWebPlayer = 1 << 3,
            OSXDashboardPlayer = 1 << 4,
            WindowsWebPlayer = 1 << 5,
            WindowsEditor = 1 << 6,
            IPhonePlayer = 1 << 7,
            PS3 = 1 << 1 << 8,
            XBOX360 = 1 << 9,
            Android = 1 << 10,
            NaCl = 1 << 11,
            LinuxPlayer = 1 << 12,
            FlashPlayer = 1 << 13,
            LinuxEditor = 1 << 14,
            WebGLPlayer = 1 << 15,
            MetroPlayerX86 = 1 << 16,
            WSAPlayerX86 = 1 << 17,
            MetroPlayerX64 = 1 << 18,
            WSAPlayerX64 = 1 << 19,
            MetroPlayerARM = 1 << 20,
            WSAPlayerARM = 1 << 21,
            WP8Player = 1 << 22,
            BB10Player = 1 << 23,
            BlackBerryPlayer = 1 << 24,
            TizenPlayer = 1 << 25,
            PSP2 = 1 << 26,
            PS4 = 1 << 27,
            PSM = 1 << 28,
            XboxOne = 1 << 29,
            SamsungTVPlayer = 1 << 30,
            WiiU = 1 << 31,
            tvOS = 1 << 32,
            Switch = 1 << 33,
            Lumin = 1 << 34,
            Nothing = 0,
            Everything = ~0
        }

        public static Platform Make(ControllerType controllerDef)
        {
            Platform newPlatform = CreateInstance<Platform>();
            newPlatform.owningControllerDef = controllerDef;
            return newPlatform;
        }

        [SerializeField]
        private List<InputDefinition> inputs = new List<InputDefinition>();

        private void OnEnable()
        {
            hideFlags = HideFlags.HideInHierarchy;
        }

        public Platform Clone()
        {
            Platform platform = Make(owningControllerDef);
            platform.m_platform = this.m_platform;
            foreach (InputDefinition input in inputs)
            {
                if (input != null)
                    platform.inputs.Add(input.Clone());
            }
            platform.name = "Platform";
#if UNITY_EDITOR
            UnityEditor.AssetDatabase.AddObjectToAsset(platform, this);
            UnityEditor.AssetDatabase.SaveAssets();
#endif
            return platform;
        }

        public InputDefinition[] GetInputs()
        {
            return inputs.ToArray();
        }

        public static platforms UnityPlatformEnumConversion(RuntimePlatform platform)
        {
            string platformString = platform.ToString();
            platforms result = platforms.Nothing;
            System.Enum.TryParse<platforms>(platformString, out result);
            return result;
        }

        public void Destroy()
        {
            int numInputs = inputs.Count;
            for (int index = 0; index < numInputs; index++)
            {
                InputDefinition currentInput = inputs[index];
                DestroyImmediate(currentInput, true);
            }
            DestroyImmediate(this, true);
        }
    }
}