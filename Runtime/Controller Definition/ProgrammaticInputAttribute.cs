﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using ABXY.Reflex.InputRuntime;

namespace ABXY.Reflex
{
    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class ProgrammaticInputAttribute : System.Attribute
    {
        public bool showAxisSelector { get; private set; }
        public bool showPositiveButtonSelector { get; private set; }
        public bool showNegativeButtonSelector { get; private set; }
        public bool showGravity { get; private set; }
        public bool showDeadZone { get; private set; }
        public bool showSnap { get; private set; }
        public bool showInvert { get; private set; }
        public bool showSensitivity { get; private set; }

        private static List<ProgrammaticAxisInfo> m_allDelegates = null;

        public static List<ProgrammaticAxisInfo> allDelegates
        {
            get
            {
                if (m_allDelegates == null)
                    m_allDelegates = GetAllDelegates();
                return m_allDelegates;
            }
        }

        public static List<string> allDelegateNames
        {
            get
            {
                List<string> delegateNames = new List<string>();
                foreach (ProgrammaticAxisInfo axisDel in allDelegates)
                {
                    delegateNames.Add(axisDel.delegateName);
                }
                return delegateNames;
            }
        }

        public static int FindIndexOfDelegate(Delegates.ProgrammaticAxisDel del)
        {
            for (int index = 0; index < allDelegates.Count; index++)
            {
                if (allDelegates[index].ProgrammaticAxisDel == del)
                    return index;
            }
            return -1;
        }

        public static int FindIndexOfProgrammaticInput(ProgrammaticAxisInfo info)
        {
            for (int index = 0; index < allDelegates.Count; index++)
            {
                if (allDelegates[index].Equals(info))
                    return index;
            }
            return -1;
        }

        /// <summary>
        /// Expects a method identifier formatted as Classname/Single Methodname(int arg1, int arg2)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static ProgrammaticAxisInfo FindAxisByName(string name)
        {
            foreach (ProgrammaticAxisInfo axis in allDelegates)
            {
                string identifier = axis.ProgrammaticAxisDel.Method.ReflectedType.Name + "/" + axis.ProgrammaticAxisDel.Method.Name;
                if (identifier == name)
                    return axis;
            }
            return new ProgrammaticAxisInfo();
        }

        public static ProgrammaticAxisInfo GetProgrammaticAxisByIndex(int index)
        {
            return allDelegates[index];
        }

        public ProgrammaticInputAttribute(
            bool showAxisSelector = false,
            bool showPositiveButtonSelector = false,
            bool showNegativeButtonSelector = false,
            bool showGravity = false,
            bool showDeadZone = false,
            bool showSnap = false,
            bool showInvert = false,
            bool showSensitivity = false)
        {
            this.showAxisSelector = showAxisSelector;
            this.showPositiveButtonSelector = showPositiveButtonSelector;
            this.showNegativeButtonSelector = showNegativeButtonSelector;
            this.showGravity = showGravity;
            this.showDeadZone = showDeadZone;
            this.showSnap = showSnap;
            this.showInvert = showInvert;
            this.showSensitivity = showSensitivity;
        }

        private static List<ProgrammaticAxisInfo> GetAllDelegates()
        {

            List<ProgrammaticAxisInfo> delegates = new List<ProgrammaticAxisInfo>();
            Assembly[] assems = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assem in assems)
            {
                foreach (System.Type type in assem.GetTypes())
                {
                    foreach (MethodInfo method in type.GetMethods())
                    {
                        List<object> attributes = new List<object>(method.GetCustomAttributes());

                        foreach (object obj in attributes)
                        {
                            ProgrammaticInputAttribute attribute = obj as ProgrammaticInputAttribute;
                            if (attribute != null && method.IsStatic)
                            {
                                ProgrammaticAxisInfo axisInfo
                                    = new ProgrammaticAxisInfo(type.Name + "/" + method.Name, System.Delegate.CreateDelegate(typeof(Delegates.ProgrammaticAxisDel), method) as Delegates.ProgrammaticAxisDel,
                                    attribute);
                                delegates.Add(axisInfo);
                            }
                        }


                    }
                }
            }
            return delegates;
        }

        [System.Serializable]
        public struct ProgrammaticAxisInfo
        {
            public Delegates.ProgrammaticAxisDel ProgrammaticAxisDel { get; private set; }
            public ProgrammaticInputAttribute attributeData { get; private set; }
            public string delegateName { get; private set; }
            public ProgrammaticAxisInfo(string delegateName, Delegates.ProgrammaticAxisDel ProgrammaticAxisDel, ProgrammaticInputAttribute attributeData)
            {
                this.delegateName = delegateName;
                this.ProgrammaticAxisDel = ProgrammaticAxisDel;
                this.attributeData = attributeData;
            }
        }
    }
}


