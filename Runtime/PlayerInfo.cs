﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.EventSystem;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using ABXY.Reflex.Internal;

namespace ABXY.Reflex
{
    public partial class PlayerInfo : ScriptableObject
    {
        private static int lastPlayerNumber = 0;

        public System.Guid playerGuid { get; private set; }

        public int playerNumber { get; private set; }

        public static PlayerInfo Make()
        {
            return Make(null);
        }

        public static PlayerInfo Make(System.Action<PlayerInfo> postPrep)
        {
            PlayerInfo playerInfo = ScriptableObject.CreateInstance<PlayerInfo>();
            playerInfo.playerGuid = System.Guid.NewGuid();

            // setting player number
            playerInfo.playerNumber = lastPlayerNumber + 1;
            lastPlayerNumber++;

            // setting up events
            ControlManager.query.AddListener<ReflexEvents.GetPlayerByID>(playerInfo.GetID);
            ControlManager.events.AddListener<ReflexEvents.OnPlayerInfoDestroyed>(playerInfo.OnPlayerRemovedFromGame);

            if (postPrep != null)
                postPrep.Invoke(playerInfo);
            ControlManager.events.Raise(new ReflexEvents.OnPlayerInfoCreated(playerInfo));
            return playerInfo;
        }

        private ReflexEvents.GetPlayerByID.Answer GetID(ReflexEvents.GetPlayerByID q)
        {
            if (q.playerID == playerGuid)
                return new ReflexEvents.GetPlayerByID.Answer(this);
            return null;
        }

        public void RemoveFromGame()
        {
            ControlManager.events.Raise(new ReflexEvents.OnPlayerInfoDestroyed(this));
            lastPlayerNumber--;
            Destroy(this);
        }

        private void OnPlayerRemovedFromGame(ReflexEvents.OnPlayerInfoDestroyed e)
        {
            // decrementing my player number if a player with a smaller number has been removed
            if (e.despawnedPlayer != this && e.despawnedPlayer.playerNumber < playerNumber)
                playerNumber--;
        }

        public int[] GetAssignedControllers()
        {
            return PlayerManagerBase.GetPlayerControllerNumbers(playerGuid);
        }
    }
}
