﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.EventSystem;
namespace ABXY.Reflex.InputRuntime.ControlManagerSystem
{
    public class InputAxis : ScriptableObject
    {
        [SerializeField]
        private string axisName = "";

        public string AxisName
        {
            get { return axisName; }
        }

        public string controllerName { get; private set; }

        public LocalizedEvents inputEvents { get; private set; }

        [SerializeField]
        private Enums.InputTypes inputType = Enums.InputTypes.Button;

        [SerializeField]
        private Enums.Axes axis = Enums.Axes.AxisX;

        [SerializeField]
        private Enums.ButtonNames positive = Enums.ButtonNames.None;

        private string positiveButtonString = "";

        [SerializeField]
        private Enums.ButtonNames negative = Enums.ButtonNames.None;

        private string negativeButtonString = "";

        [SerializeField]
        private float gravity = 1f;

        [SerializeField]
        private float sensitivity = 1f;

        [SerializeField]
        private float deadZone = 0f;

        [SerializeField]
        private bool snap = false;

        [SerializeField]
        private bool invert = false;

        [SerializeField]
        private bool invertX = false;

        [SerializeField]
        private bool invertY = false;

        [SerializeField]
        private InputDefinition.AnalogButtonOperations analogButtonOperationType = InputDefinition.AnalogButtonOperations.GreaterThan;

        [SerializeField]
        private float analogButtonCutoff = 0.5f;

        private System.Enum inputEnum;

        private Delegates.ProgrammaticAxisDel customAxisDel;


        //at the iterations we are doing, this is a performance hit!
        private string axisTostring = "";

        public static InputAxis Make(
            string name,
            Enums.InputTypes inputType,
            Enums.Axes axis,
            Enums.ButtonNames positive,
            Enums.ButtonNames negative,
            float gravity,
            float sensitivity,
            float deadZone,
            bool snap,
            bool invert,
            bool invertX,
            bool invertY,
            InputDefinition.AnalogButtonOperations analogButtonOperationsType,
            float analogButtonCutoff,
            string controllerName,
            System.Enum inputEnum,
            Delegates.ProgrammaticAxisDel customAxisDel)
        {
            InputAxis input = CreateInstance<InputAxis>();
            input.axisName = name;
            input.inputType = inputType;
            input.axis = axis;
            input.positive = positive;
            input.positiveButtonString = positive.ToString();
            input.negative = negative;
            input.negativeButtonString = negative.ToString();
            input.gravity = gravity;
            input.sensitivity = sensitivity;
            input.deadZone = deadZone;
            input.snap = snap;
            input.invert = invert;
            input.invertX = invertX;
            input.invertY = invertY;
            input.analogButtonOperationType = analogButtonOperationsType;
            input.analogButtonCutoff = analogButtonCutoff;
            input.controllerName = controllerName;
            input.inputEnum = inputEnum;
            input.customAxisDel = customAxisDel;
            input.inputEvents = new LocalizedEvents();
            input.axisTostring = axis.ToString();
            return input;
        }

        [SerializeField]
        private float m_axisValue = 0f;
        public float axisValue { get { return m_axisValue; } }

        [SerializeField]
        private Vector3 m_vectorAxisValue = Vector3.zero;
        public Vector3 vectorAxisValue { get { return m_vectorAxisValue; } }

        [SerializeField]
        public bool buttonState { get { return axisValue > 0.5f; } }

        public void UpdateInput(int joystickNumber)
        {
            switch (inputType)
            {
                case Enums.InputTypes.AnalogAxis:
                    DoAnalogAxis(joystickNumber);
                    break;
                case Enums.InputTypes.Button:
                    DoButtonAxis(joystickNumber);
                    break;
                case Enums.InputTypes.DigitalAxis:
                    DoDigitalAxis(joystickNumber);
                    break;
                case Enums.InputTypes.MouseAxis:
                    m_vectorAxisValue.x = Input.GetAxis("MouseX");
                    m_vectorAxisValue.y = Input.GetAxis("MouseY");
                    break;
                case Enums.InputTypes.AnalogButton:
                    DoAnalogButton(joystickNumber);
                    break;
                case Enums.InputTypes.ProgrammaticInput:
                    DoScriptedAxis(joystickNumber);
                    break;
            }

        }

        /// <summary>
        /// Reads this input as an analog axis, and updates this axis' values appropriately
        /// </summary>
        /// <param name="joystickNumber"></param>
        private void DoAnalogAxis(int joystickNumber)
        {
            float newAxisValue = Input.GetAxis("Joystick" + (joystickNumber + 1) + axisTostring);

            //doing invert
            if (invert)
                newAxisValue = -newAxisValue;

            //Doing Deadzone
            if (Mathf.Abs(newAxisValue) < deadZone)
                newAxisValue = 0f;

            //doing sensitivity
            newAxisValue *= sensitivity;

            // applying the new values
            
            m_axisValue = newAxisValue;
            inputEvents.Raise(new ReflexEvents.OnInputChange(new InputChange(m_axisValue, buttonState, Vector3.zero), controllerName, inputEnum, joystickNumber));
            
            m_axisValue = newAxisValue;
        }

        private void DoAnalogButton(int joystickNumber)
        {
            float newAxisValue = Input.GetAxis("Joystick" + (joystickNumber + 1) + axisTostring);
            bool active = false;
            switch (analogButtonOperationType)
            {
                case InputDefinition.AnalogButtonOperations.Equal:
                    active = newAxisValue == analogButtonCutoff;
                    break;
                case InputDefinition.AnalogButtonOperations.GreaterThan:
                    active = newAxisValue > analogButtonCutoff;
                    break;
                case InputDefinition.AnalogButtonOperations.GreaterThanOrEqual:
                    active = newAxisValue >= analogButtonCutoff;
                    break;
                case InputDefinition.AnalogButtonOperations.LessThan:
                    active = newAxisValue < analogButtonCutoff;
                    break;
                case InputDefinition.AnalogButtonOperations.LessThanOrEqual:
                    active = newAxisValue <= analogButtonCutoff;
                    break;
            }
            if (active)
                m_axisValue = 1f;
            else
                m_axisValue = 0f;

        }

        private void DoDigitalAxis(int joystickNumber)
        {
            float positiveInput = 0f;
            float negativeInput = 0f;
            if (positive != Enums.ButtonNames.None)
                positiveInput = DoButtonAxisValue(positiveButtonString, joystickNumber);
            if (negative != Enums.ButtonNames.None)
                negativeInput = DoButtonAxisValue(negativeButtonString, joystickNumber);
            float newDigitalAxisValue = positiveInput - negativeInput;

            //inverting input
            if (invert)
                newDigitalAxisValue = -newDigitalAxisValue;

            //doing gravity
            if (!snap && newDigitalAxisValue == 0f && gravity != 0f)
                newDigitalAxisValue = Mathf.MoveTowards(m_axisValue, 0, (1 / gravity) * Time.deltaTime);

            //Doing snap
            if (newDigitalAxisValue == 0 && snap)
                newDigitalAxisValue = 0f;

            // doing Sensitivity
            newDigitalAxisValue *= sensitivity;

            if (newDigitalAxisValue != m_axisValue)
            {
                m_axisValue = newDigitalAxisValue;
                inputEvents.Raise(new ReflexEvents.OnInputChange(new InputChange(m_axisValue, buttonState, Vector3.zero), controllerName, inputEnum, joystickNumber));
            }
            m_axisValue = newDigitalAxisValue;
        }

        private void DoButtonAxis(int joystickNumber)
        {
            if (positive != Enums.ButtonNames.None)
            {
                bool previousButtonValue = buttonState;
                m_axisValue = DoButtonAxisValue(positiveButtonString, joystickNumber);
                if (previousButtonValue != buttonState)
                    inputEvents.Raise(new ReflexEvents.OnInputChange(new InputChange(m_axisValue, buttonState, Vector3.zero), controllerName, inputEnum, joystickNumber));
            }
        }

        private void DoScriptedAxis(int joystickNumber)
        {
            if (customAxisDel != null)
            {
                InputChange result = customAxisDel(joystickNumber, axis, positive, negative, gravity, sensitivity, deadZone, analogButtonCutoff, snap, invert);
                m_axisValue = result.axisValue;
                m_vectorAxisValue = result.vectorValue;
                inputEvents.Raise(new ReflexEvents.OnInputChange(result, controllerName, inputEnum, joystickNumber));
            }
        }

        private float DoButtonAxisValue(string buttonName, int joystickNumber)
        {
            bool isJoystickButton = (int)positive >= (int)Enums.ButtonNames.joystickButton0;
            if (isJoystickButton)
                return Input.GetAxis("Joystick" + (joystickNumber + 1) + buttonName);
            else
                return Input.GetAxis(buttonName.ToString());
        }



        private string TransformButtonName(Enums.ButtonNames buttonEnum)
        {
            switch (buttonEnum)
            {
                case Enums.ButtonNames.zero:
                    return "0";
                case Enums.ButtonNames.one:
                    return "1";
                case Enums.ButtonNames.two:
                    return "2";
                case Enums.ButtonNames.three:
                    return "3";
                case Enums.ButtonNames.four:
                    return "4";
                case Enums.ButtonNames.five:
                    return "5";
                case Enums.ButtonNames.six:
                    return "6";
                case Enums.ButtonNames.seven:
                    return "7";
                case Enums.ButtonNames.eight:
                    return "8";
                case Enums.ButtonNames.nine:
                    return "9";
            }
            return buttonEnum.ToString();
        }

        public override string ToString()
        {
            return controllerName + " + " + axisName + " | " + inputEvents.Count + " subscribers";
        }
    }

}