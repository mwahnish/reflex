﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using System.Linq;
using ABXY.EventSystem;

namespace ABXY.Reflex
{
    public partial class Controller : ScriptableObject
    {
        public int controllerNumber { get; private set; }
        public ControllerType type { get; private set; }

        [SerializeField]
        private List<InputAxis> inputsList = new List<InputAxis>();
        private Dictionary<string, InputAxis> inputsDict = new Dictionary<string, InputAxis>();//TODO remove
        private System.Guid controllerGuid = System.Guid.NewGuid();

        public string controllerName
        {
            get
            {
                if (type != null)
                    return type.name;
                else
                    return "None";
            }
        }

        public static Controller Make(ControllerType controllerType, int controllerNumber)
        {
            Controller newController = CreateInstance<Controller>();
            newController.controllerNumber = controllerNumber;
            newController.type = controllerType;
            Platform.platforms currentPlatform = Platform.UnityPlatformEnumConversion(Application.platform);
            InputDefinition[] inputs = controllerType.GetInputs(currentPlatform);

            System.Reflection.Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            Type inputEnumType = null;
            foreach (System.Reflection.Assembly assem in assemblies)
            {
                Type[] types = assem.GetTypes();
                foreach (Type type in types)
                {
                    if (type.Namespace == "Controllers" && type.IsEnum && type.Name == controllerType.SanitizedName)
                        inputEnumType = type;
                }
            }


            foreach (InputDefinition input in inputs)
            {
                InputAxis clonedInput = input.MakeAxis(newController.controllerName, inputEnumType);
                if (!newController.inputsDict.ContainsKey(clonedInput.AxisName))
                {
                    newController.inputsList.Add(clonedInput);
                    newController.inputsDict.Add(clonedInput.AxisName, clonedInput);
                }
            }

            ControlManager.query.AddListener<Reflex.Internal.ReflexInternalEvents.GetControllerReference>(newController.GetControllerReference);
            return newController;
        }

        public void SubscribeToInput(System.Enum input, Events.EventDelegate<ReflexEvents.OnInputChange> onInputChange)
        {
            string axisSearchName = input.ToString().Replace(" ", "").ToLower(); ;
            string controllerSearchName = input.GetType().Name.Replace(" ", "").ToLower(); ;
            InputAxis selectedAxis = null;
            foreach (InputAxis axis in inputsList) {
                if (axis.controllerName.Replace(" ", "").ToLower() == controllerSearchName && axis.AxisName.Replace(" ", "").ToLower() == axisSearchName)
                {
                    selectedAxis = axis;
                    break;
                }

            }
            if (selectedAxis != null)
                selectedAxis.inputEvents.AddListener<ReflexEvents.OnInputChange>(onInputChange);
        }

        public void UnsubscribeToInput( Events.EventDelegate<ReflexEvents.OnInputChange> onInputChange)
        {
            foreach(InputAxis axis in inputsList)
                axis.inputEvents.RemoveListener<ReflexEvents.OnInputChange>(onInputChange);
        }

        public void UpdateInputs()
        {
            foreach (InputAxis input in inputsList)
            {
                input.UpdateInput(controllerNumber);
            }
        }

        public Reflex.Internal.ReflexInternalEvents.GetControllerReference.Answer GetControllerReference(Reflex.Internal.ReflexInternalEvents.GetControllerReference q)
        {
            if (q.controllerNumber == controllerNumber)
                return new Internal.ReflexInternalEvents.GetControllerReference.Answer(this);
            return null;
        }

        public override string ToString()
        {
            return controllerName + " (" + controllerNumber + ") |(" + controllerGuid.ToString()+")"; 
        }

        public void Kill()
        {
            ControlManager.query.RemoveListener<Reflex.Internal.ReflexInternalEvents.GetControllerReference>(GetControllerReference);
        }
    }
}