﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.EventSystem;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using abxy.editorhelpers;
using ABXY.Reflex.Internal;
using UnityEngine.SceneManagement;

namespace ABXY.Reflex
{
    public partial class ControlManager : MonoBehaviour
    {
        [SerializeField]
        private List<ControllerType> allowedControllerTypes = new List<ControllerType>();

        [SerializeField]
        private List<ControllerType> inherentControllerTypes = new List<ControllerType>();

        [SerializeField]
        private List<Controller> availableControllers = new List<Controller>();

        [SerializeField]
        private List<string> undefinedControllers = new List<string>();

        private Dictionary<Controller, int> controllersJoyNumbers = new Dictionary<Controller, int>();

        [SerializeField]
        private bool persistAcrossScenes = false;

        [SerializeField] private bool supersedesOthers = false;

        [SerializeField]
        [AutoValidator(LeftComparators.objectReferenceValue,
            Comparators.NOT_EQUALS,
            null,
            messageType = validatorMessageTypes.ERROR,
            message = "Player manager must be assigned")]
        private PlayerManagerBase playerManager = null;

        static LocalizedEvents eventsInternal = null;
        public static LocalizedEvents events
        {
            get
            {
                if (eventsInternal == null)
                {
                    eventsInternal = new LocalizedEvents();
                }

                return eventsInternal;
            }
        }

        static LocalizedQuery queryInternal = null;
        public static LocalizedQuery query
        {
            get
            {
                if (queryInternal == null)
                {
                    queryInternal = new LocalizedQuery();
                }

                return queryInternal;
            }
        }

        private static ControlManager instance;

        public static PlayerInfo[] currentPlayers
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<ControlManager>();
                if (instance != null && instance.playerManager != null)
                    return instance.playerManager.GetPlayers();
                return new PlayerInfo[0];
            }
        }

        public static void AddOnPlayerSpawnedListener(EventSystem.Events.EventDelegate<ReflexEvents.OnPlayerInfoCreated> OnPlayerSpawned)
        {
            events.AddListener<ReflexEvents.OnPlayerInfoCreated>(OnPlayerSpawned);
        }

        public static void RemoveOnPlayerSpawnedListener(EventSystem.Events.EventDelegate<ReflexEvents.OnPlayerInfoCreated> OnPlayerSpawned)
        {
            events.AddListener<ReflexEvents.OnPlayerInfoCreated>(OnPlayerSpawned);
        }

        public static void AddOnPlayerDeSpawnedListener(EventSystem.Events.EventDelegate<ReflexEvents.OnPlayerInfoDestroyed> OnPlayerDeSpawned)
        {
            events.AddListener<ReflexEvents.OnPlayerInfoDestroyed>(OnPlayerDeSpawned);
        }

        public static void RemoveOnPlayerDeSpawnedListener(EventSystem.Events.EventDelegate<ReflexEvents.OnPlayerInfoDestroyed> OnPlayerDeSpawned)
        {
            events.AddListener<ReflexEvents.OnPlayerInfoDestroyed>(OnPlayerDeSpawned);
        }

        private void Awake()
        {
            ControlManager[] preexistingControlManager = FindObjectsOfType<ControlManager>();
            if (supersedesOthers)
            {
                for (int index = 0; index < preexistingControlManager.Length; index++)
                {
                    ControlManager controlManager = preexistingControlManager[index];
                    if (controlManager != this)
                        Destroy(controlManager.gameObject);
                }
            }
            else // meaning, another exists
            {
                bool foundOthers = false;
                foreach (var controlManager in preexistingControlManager)
                {
                    if (controlManager != this)
                        foundOthers = true;
                }
                if (foundOthers)
                    Destroy(this.gameObject);
            }
        }

        private void Start()
        {

            query.AddListener<ReflexInternalEvents.GetAllControllers>(GetAllControllers);
            if (playerManager != null)
                playerManager.Setup();
            SetupInherentControllers();
        }




        private void Update()
        {
            if (persistAcrossScenes)
                DontDestroyOnLoad(gameObject);
            else
                SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());

            string[] joystickNames = Input.GetJoystickNames();

            // Iterating through the controller strings presented by unity, and updating the available controllers as appropriate
            for (int index = 0; index < joystickNames.Length; index++)
            {
                // adding and removing controllers as necessary
                ControllerType controllerType = CheckIfControllerTypeIsAvailable(joystickNames[index]);
                SetUnsetControllerAvailabilty(index, controllerType);

                // If there is no definition for the controller available, this function will add it
                ManageUndefinedControllersList(joystickNames[index], controllerType);
            }

            foreach (Controller controller in availableControllers)
            {
                controller.UpdateInputs();
            }
            if (playerManager != null)
                playerManager.Update();
        }

        private void SetUnsetControllerAvailabilty(int joystickNumber, ControllerType controllerType)
        {
            Controller controller = availableControllers.Find(x => x.controllerNumber == joystickNumber);
            if (controllerType == null && controller != null) // in other words, the controller was connected, but is now disconnected
            {
                availableControllers.Remove(controller);
                controllersJoyNumbers.Remove(controller);
                events.Raise(new ReflexInternalEvents.OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange.Status.DISCONNECTED, joystickNumber));
                controller.Kill();
                Destroy(controller);
                return;
            }
            else if (controller != null && controller.type != controllerType) // in other words, the controller was connected, but has been replaced with a new controller
            {
                availableControllers.Remove(controller);
                controllersJoyNumbers.Remove(controller);
                events.Raise(new ReflexInternalEvents.OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange.Status.DISCONNECTED, joystickNumber));
                controller.Kill();
                Destroy(controller);

                Controller newController = Controller.Make(controllerType, joystickNumber);
                controllersJoyNumbers.Add(newController, joystickNumber);
                availableControllers.Add(newController);

                events.Raise(new ReflexInternalEvents.OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange.Status.CONNECTED, joystickNumber));
                return;
            }

            controller = availableControllers.Find(x => x.controllerNumber == joystickNumber && x.type == controllerType);
            if (controller == null && controllerType != null) // in other words, the controller was not connected, but now it is
            {
                Controller newController = Controller.Make(controllerType, joystickNumber);
                controllersJoyNumbers.Add(newController, joystickNumber);
                availableControllers.Add(newController);
                events.Raise(new ReflexInternalEvents.OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange.Status.CONNECTED, joystickNumber));
            }


        }

        private void SetupInherentControllers()
        {
            foreach (ControllerType controllerType in inherentControllerTypes)
            {
                int joyNumber = Random.Range(100, 200);
                int safety = 0;
                while (controllersJoyNumbers.ContainsValue(joyNumber))
                {
                    joyNumber = Random.Range(100, 200);
                    safety++;
                    if (safety > 200) // then I'm probably out of joystick numbers
                        return;
                }

                Controller newController = Controller.Make(controllerType, joyNumber);
                controllersJoyNumbers.Add(newController, joyNumber);
                availableControllers.Add(newController);
                events.Raise(new ReflexInternalEvents.OnControllerStatusChange(ReflexInternalEvents.OnControllerStatusChange.Status.CONNECTED, joyNumber));
            }
        }

        private ReflexInternalEvents.GetAllControllers.Answer GetAllControllers(ReflexInternalEvents.GetAllControllers e)
        {
            return new ReflexInternalEvents.GetAllControllers.Answer(controllersJoyNumbers);
        }

        private void ManageUndefinedControllersList(string controllerName, ControllerType controllerType)
        {
            // "" Just means no controller is connected - we don't need to add to the list in that case
            if (controllerType == null && controllerName != "" && !undefinedControllers.Contains(controllerName))
            {
                undefinedControllers.Add(controllerName);
            }
        }

        private ControllerType CheckIfControllerTypeIsAvailable(string controllerName)
        {
            foreach (ControllerType controllerType in allowedControllerTypes)
            {
                if (controllerType != null && controllerType.MatchesControllerNameString(controllerName))
                    return controllerType;
            }
            return null;
        }

        public PlayerManagerType GetPlayerManager<PlayerManagerType>()where PlayerManagerType:PlayerManagerBase
        {
            return (PlayerManagerType)playerManager;
        }

        private void OnDestroy()
        {
            foreach (Controller controller in availableControllers)
                controller.Kill();
            if (playerManager != null)
                playerManager.Destroy();
            //events.Clear();
            //query.Clear();
        }


    }
}