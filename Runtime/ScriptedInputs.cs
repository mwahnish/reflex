﻿using ABXY.Reflex.InputRuntime.ControllerDefinitionSystem;
using ABXY.Reflex.InputRuntime.ControlManagerSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Reflex;

namespace ABXY.Reflex.InputRuntime
{
    public class ScriptedInputs : MonoBehaviour
    {

        [ProgrammaticInput(showSensitivity: true)]
        public static InputChange MousePosition(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            Vector2 mousePosition = Input.mousePosition;
            return new InputChange(0f, false, mousePosition);
        }

        private static Vector2 lastMousePosition = Vector2.zero;
        [ProgrammaticInput(showSensitivity: true)]
        public static InputChange MouseDelta(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            Vector2 mousePosition = Input.mousePosition;
            Vector2 delta = mousePosition-lastMousePosition;
            lastMousePosition = mousePosition;
            return new InputChange(0f, false, delta);
        }

        [ProgrammaticInput()]
        public static InputChange MouseButton0(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            bool buttonDown = Input.GetMouseButton(0);
            return new InputChange(buttonDown ? 1 : 0, buttonDown, Vector3.zero);
        }

        [ProgrammaticInput()]
        public static InputChange MouseButton1(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            bool buttonDown = Input.GetMouseButton(1);
            return new InputChange(buttonDown ? 1 : 0, buttonDown, Vector3.zero);
        }


        [ProgrammaticInput()]
        public static InputChange MouseButton2(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            bool buttonDown = Input.GetMouseButton(2);
            return new InputChange(buttonDown ? 1 : 0, buttonDown, Vector3.zero);
        }


        [ProgrammaticInput()]
        public static InputChange MouseButton3(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            bool buttonDown = Input.GetMouseButton(3);
            return new InputChange(buttonDown ? 1 : 0, buttonDown, Vector3.zero);
        }


        [ProgrammaticInput()]
        public static InputChange MouseButton4(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            bool buttonDown = Input.GetMouseButton(4);
            return new InputChange(buttonDown ? 1 : 0, buttonDown, Vector3.zero);
        }


        [ProgrammaticInput()]
        public static InputChange MouseButton5(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            bool buttonDown = Input.GetMouseButton(5);
            return new InputChange(buttonDown ? 1 : 0, buttonDown, Vector3.zero);
        }


        [ProgrammaticInput()]
        public static InputChange MouseButton6(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            bool buttonDown = Input.GetMouseButton(6);
            return new InputChange(buttonDown ? 1 : 0, buttonDown, Vector3.zero);
        }

        

        [ProgrammaticInput()]
        public static InputChange Scroll(int joystickNumber,
        Enums.Axes axis,
        Enums.ButtonNames positive,
        Enums.ButtonNames negative,
        float gravity,
        float sensitivity,
        float deadZone,
        float analogButtonCutoff,
        bool snap,
        bool invert)
        {
            Vector3 scrollValue = Input.mouseScrollDelta;
            return new InputChange(0, false, scrollValue);
        }
    }
}
